(function () {

	/**
	 * @private
	 * @constant
	 * The not started task status
	 */
	var CATEGORY_NOT_STARTED = 'Not Started';

	/**
	 * @private
	 * @constant
	 * The in progress task status
	 */
	var CATEGORY_IN_PROGRESS = 'In Progress';

	/**
	 * @private
	 * @constant
	 * The complete task status
	 */
	var CATEGORY_COMPLETED = 'Completed';

	/**
	 * @private
	 * @constant
	 * The blocked task status
	 */
	var CATEGORY_BLOCKED = 'Blocked';

	/**
	 * @private
	 * Takes a variable number of arguments and joins them together separated
	 * with new line characters.
	 * @return {String} The joined strings
	 */
	function lines () {
		var text = '',
			i;

		for ( i = 0; i < arguments.length; i++ ) {
			text += arguments[i] + '\n';
		}

		return text;
	}

	window.KanbanRenderer = {

		/**
		 * Renders the kanban board with the provided features and tasks.
		 * @param {Object[]} features An array of features to be displayed
		 */
		renderBoard: function ( features ) {
			var html = lines(
				'<table id="sprintWall">',
					'<thead>',
						'<tr class="headerRow">',
							'<th class="controlCol"></th>',
							'<th class="storyCol">Feature</th>',
							'<th id="notStartedH">Not Started</th>',
							'<th id="inProgressH">In Progress</th>',
							'<th id="completeH">Complete</th>',
							'<th id="blockedH">Blocked</th>',
						'</tr>',
					'</thead>',
					'<tbody>'
				);

			$j.each( features, function( index, feature ) {
				var categories = {};

				categories[CATEGORY_NOT_STARTED] = [];
				categories[CATEGORY_IN_PROGRESS] = [];
				categories[CATEGORY_COMPLETED] = [];
				categories[CATEGORY_BLOCKED] = [];

				$j.each( feature.Tasks, function( index, task ) {
					task.Status = task.Status || CATEGORY_IN_PROGRESS;
					categories[task.Status].push(task);
				});

				html += lines(
						'<tr class="contracted" id="' + feature.Id + '">',
							'<td>',
								'<a class="expandContract" href="" title="Expand">',
									'<img src="' + $Params.ArrowOutImageUrl + '" alt="" />',
								'</a>',
							'</td>',
							'<td>',
								KanbanRenderer.renderFeature(feature),
							'</td>',
							KanbanRenderer.getTasksAsHtml(feature, categories[CATEGORY_NOT_STARTED], 'notStarted'),
							KanbanRenderer.getTasksAsHtml(feature, categories[CATEGORY_IN_PROGRESS], 'inProgress'),
							KanbanRenderer.getTasksAsHtml(feature, categories[CATEGORY_COMPLETED], 'complete'),
							KanbanRenderer.getTasksAsHtml(feature, categories[CATEGORY_BLOCKED], 'blocked'),
						'</tr>'
					);
			});

			html += '</tbody></table>';

			$j('#main').append(html);
		},

		/**
		 * Renders the provided feature as HTML.
		 * @param {Object} feature The feature to render
		 * @return {String} The rendered HTML
		 */
		renderFeature: function ( feature ) {
			var html = '';

			function prop ( key, value ) {
				html += '<div class="' + key + '">' + value + '</div>';
			}

			prop('number', feature.Name);
			prop('name', feature.Label);

			if ( feature.Description ) {
				prop('desc', feature.Description);
				html += '<br/>';
			}

			prop('pointsalloc', feature.Status);

			prop('action view',
				'<a href="/' + feature.Id + '" target="_blank" title="View"><img src="' + $Params.ZoomImageUrl + '" alt="" /></a>'
			);

			prop('action add',
				'<a href="#" title="Add task"><img src="' + $Params.AddImageUrl + '" alt="Add" /></a>'
			);

			if ( feature.Status === KanbanController.constants.FEATURE_STATUS_PLANNING ) {
				prop('action start-feature',
					'<a href="#" title="Start Feature"><img src="' + $Params.StartImageUrl + '" alt="Start" /></a>'
				);
			}

			if ( feature.Status === KanbanController.constants.FEATURE_STATUS_IN_PROGRESS ) {
				prop('action complete-feature',
					'<a href="#" title="Complete Feature"><img src="' + $Params.CompleteImageUrl + '" alt="Complete" /></a>'
				);
			}

			return '<div class="story">' + html + '</div>';
		},

		/**
		 * Renders the provided task as HTML.
		 * @param {Object} task The task to render as HTML
		 * @param {Object} feature The parent feature of the task to render
		 * @return {String} The rendered HTML
		 */
		renderTask: function ( task, feature ) {
			var html = '',
				owner = task.AssignedName || 'No owner',
				categories = {
					Development: 'development',
					Design: 'design',
					Analysis: 'analysis',
					QA: 'qa',
					Documentation: 'documentation',
					Bug: 'bug'
				};

			function prop ( key, value ) {
				html += '<div class="' + key + '">' + value + '</div>';
			}

			function br() {
				html += '<br/>';
			}

			prop('name', task.Name);
			br();

			if ( task.Description ) {
				prop('description', task.Description);
			}

			prop('taskStoryId', feature.Id);

			prop('ownerinfo',
				'<img src="' + $Params.UserAddImageUrl + '" alt="" /><div class="owner" data-recordid="' + task.Assigned + '">' + owner + '</div>'
			);

			prop('estimate', task.Estimate);
			prop('remaining', task.TotalHours);

			prop('edit',
				'<a href="#" title="Edit"><img src="' + $Params.CogImageUrl + '" alt="" /></a>'
			);

			prop('view',
				'<a href="/' + task.Id + '" target="_blank" title="View"><img src="' + $Params.ZoomImageUrl + '" alt="" /></a>'
			);

			return '<div id="' + task.Id + '" class="task ' + (categories[task.Category] || 'nocategory') + '" data-feature="' + feature.Id + '">' + html + '</div>';
		},

		/**
		 * @private
		 * Takes an array of tasks to be converted into table cell elements.
		 * @param {Object} feature The feature that owns the tasks being rendered
		 * @param {Object[]} tasks An array of TaskSummary objects
		 * @param {String} css A CSS classname to apply to the generated <td> element
		 * @return {String} The rendered HTML
		 */
		getTasksAsHtml: function ( feature, tasks, css ) {
			var html = '';

			$j.each( tasks, function ( i, task ) {
				html += KanbanRenderer.renderTask(task, feature);
			});

			return '<td class="statusList ' + css + '">' + html + '</td>';
		}
	};

})();
