(function () {

	function showError ( err ) {
		$j("#error").attr( 'title', $Label.KanbanErrorTitle ).html( "<p>" + err + "</p>" ).dialog({
			height: 140,
			modal: true,
			draggable: false,
			resizable: false,
			open: function(event, ui) { $j(".ui-dialog-titlebar-close").show(); }
		});
	}

	function showMsg ( title, msg, isBusy ) {
		if( isBusy ) msg = '<img style="vertical-align:middle;" src="/img/loading.gif" alt="" /> ' + msg;
		$j('#msg').attr( 'title', title ).html( '<p>' + msg + '</p>' ).dialog({
			autoOpen: true,
			height: 140,
			modal: true,
			draggable: false,
			resizable: false,
			closeOnEscape: false,
			open: function(event, ui) { $j(".ui-dialog-titlebar-close").hide(); }
		}).dialog('open');
	}

	function hideMsg () {
		$j('#msg').dialog( 'close' );
	}

	function expandOrContract () {
		var $me = $j(this),
			$ancestor = $me.parent().parent(),
			expanded;

		if ( $me.attr('title') == 'Expand' ) {
			$me.attr('title', 'Contract');
			$me.find('img').attr('src', $Params.ArrowInImageUrl );
			$ancestor.removeClass('contracted');
			expanded = true;
		} else {
			$me.attr('title', 'Expand');
			$me.find('img').attr('src', $Params.ArrowOutImageUrl );
			$ancestor.addClass('contracted');
			expanded = false;
		}

		storyExpanded($ancestor, expanded);

		return false;
	}

	function storyExpanded ( storyElement, expanded ) {
		var $db = $j('#db'),
			state = $db.data('storyState');

		state = state === undefined ? [] : state;
		state[storyElement.attr('id')] = expanded;

		$db.data('storyState', state);
	}

	function isStoryExpanded ( storyId ) {
		var state = $j('#db').data('storyState'),
			storyState;

		if ( state === undefined ) {
			return false;
		}

		storyState = state[storyId];
		return storyState === undefined ? false : storyState;
	}

	/**
	 * @private
	 * Invokes the specified remote action with the provided arguments. If an
	 * error is encountered then it is handled and the callback is not called.
	 * @param {String} action The remote action to call
	 * @param {Object[]} args The remote action arguments
	 * @param {Function} callback A function that will be called with the result
	 */
	function remoteAction ( action, args, callback ) {
		var manager = Visualforce.remoting.Manager,
			callArgs = [action];

		function handler ( result, event ) {
			if ( !event.status ) {
				return showError(event.message);
			}

			callback(result);
		}

		if ( args && !callback && args instanceof Function ) {
			callback = args;
		} else {
			callArgs.push.apply(callArgs, args);
		}

		callArgs.push(handler);

		manager.invokeAction.apply(manager, callArgs);
	}

	/**
	 * @private
	 * Takes a user DTO and returns HTML for a <select> element.
	 * @param {Object} user A DTO containing the user information
	 * @param {String} user.Id The Salesforce ID for the user
	 * @param {String} user.Name The name of the user
	 * @return {String} The HTML representing the user in a <select> element
	 */
	function userToHtml ( user ) {
		return '<option value="' + user.Id + '">' + user.Name + '</option>';
	}

	/**
	 * @private
	 * Loads all of the users that can be assigned to tasks and renders them into
	 * the user <select> element.
	 * @param {String} projectId The Salesforce ID for the project whose users are to be loaded
	 */
	function loadUsers ( projectId ) {
		remoteAction( $RemoteAction.KanbanController.getUsers, [projectId], function ( users ) {
			var html = '<option value="">-- None --</option>',
				i;

			for ( i = 0; i < users.length; i++ ) {
				html += userToHtml( users[i] );
			}

			$j('#taskOwner').html(html);
		});
	}

	window.KanbanController = {

		constants: Object.freeze({
			FEATURE_STATUS_PLANNING: 'Planning',
			FEATURE_STATUS_IN_PROGRESS: 'In Progress'
		}),

		/**
		 * Sends a Remote Action request to Salesforce to load all of the Project__c
		 * identities, once loaded the project select option will be populated with
		 * the project identities for the user to select from.
		 */
		loadProjects: function () {
			remoteAction( $RemoteAction.KanbanController.getProjects, function ( result ) {
				var $projects = $j('#projectList');

				$projects.removeAttr( 'disabled' ).find('option').remove().end();

				if( result.length == 0 ) {
					$projects.append( '<option value="nowt">No Projects</option>' ).attr( 'disabled', 'disabled' );
					return;
				}

				$projects.append('<option value="" selected>Choose a Project</option>');
				$j.each( result, function ( index, project ) {
					$projects.append('<option value="' + project.Id + '" data-version="' + project.Version + '">' + project.Name + '</option>');
				});
			});
		},

		/**
		 * Loads the features for the currently selected project from Salesforce.
		 */
		loadFeatures: function () {
			var projectId = $j('#projectList').val(),
				versionId = $j('#projectList').find('option[value=' + projectId + ']').data('version');

			if ( projectId == '' ) {
				return;
			}

			$j('#main').empty();
			showMsg($Label.Loading, $Label.KanbanProjectLoadingMessage, true);

			// Load the users for the project and all of the features and tasks
			loadUsers(projectId);
			remoteAction( $RemoteAction.KanbanController.getFeatures, [projectId, versionId], function ( result ) {
				$j('#db').removeData( 'taskState' );

				if ( result.length == 0 ) {
					$j('#main').append('<p>' + $Label.KanbanNoTasksToDisplay + '</p>');
				}

				KanbanRenderer.renderBoard(result);

				KanbanController.setupDragging();
				KanbanController.setupTaskViewButton();

				$j('#sprintWall').stickyTableHeaders();
				$j('.story .add a').click( function () {
					var featureId = $j(this).parents('tr').attr('id');

					KanbanController.showForm({
						Id: null,
						Name: '',
						Description: '',
						Estimate: 0,
						TotalHours: 0,
						Category: 'Development',
						Status: 'Not Started',
						Assigned: null,
						AssignedName: '',
						Feature: featureId
					}, KanbanController.saveTask);
				});
				$j('.expandContract').click( expandOrContract );

				$j('.story .start-feature a').click(function () {
					var featureId = $j(this).parents('tr').attr('id');

					$j('#startConfirm').dialog({
						resizable: false,
						height: "auto",
						width: 400,
						modal: true,
						buttons: {
							Cancel: function () {
								$j(this).dialog('close');
							},
							Start: function () {
								$j(this).dialog('close');

								remoteAction( $RemoteAction.KanbanController.startFeature, [featureId], function ( result ) {
									KanbanController.loadFeatures();
								});
							}
						}
					});
				});

				$j('.story .complete-feature a').click(function () {
					var featureId = $j(this).parents('tr').attr('id');

					$j('#completeConfirm').dialog({
						resizable: false,
						height: "auto",
						width: 400,
						modal: true,
						buttons: {
							Cancel: function () {
								$j(this).dialog('close');
							},
							Complete: function () {
								$j(this).dialog('close');

								remoteAction( $RemoteAction.KanbanController.completeFeature, [featureId], function (result) {
									KanbanController.loadFeatures();
								});
							}
						}
					});
				});

				// we may have refreshed - let's expand any stories that were expanded before we refreshed
				$j('#sprintWall tr').each(function(index) {
					if( isStoryExpanded( $j(this).attr('id') ) ) {
						$j(this).find('a.expandContract').click();
					}
				});

				hideMsg();
			});
		},

		setupDragging: function () {
			$j('.statusList').sortable({
				revert: 0,
				connectWith: '.statusList',
				receive: function( event, ui ) {
					var sourceList = ui.sender,
						destList = ui.item.parent(),
						destinationClasses = destList.attr('class').split(/\s+/),
						statusValues = {
							notStarted: 'Not Started',
							inProgress: 'In Progress',
							complete: 'Completed',
							blocked: 'Blocked'
						},
						newStatus;

					// Check the feature IDs to make sure the task hasn't been dragged into another feature
					if ( sourceList.parent().attr('id') !== destList.parent().attr('id') ) {
						$j(ui.sender).sortable('cancel');
						return;
					}

					for ( i = 0; i < destinationClasses.length; i++ ) {
						status = statusValues[ destinationClasses[i] ] || status;
					}

					Visualforce.remoting.Manager.invokeAction(
						$RemoteAction.KanbanController.updateTaskStatus,
						ui.item.attr('id'), status,
						function ( result, event ) {
							if ( !event.status ) {
								$j(ui.sender).sortable('cancel');
								showError( event.message );
								return;
							}
						}
					);
				}
			});
		},

		setupTaskViewButton: function () {
			$j('.task .edit a').click( function() {
				var taskId = $j(this).parent().parent().attr('id');

				showMsg($Label.Loading, $Label.KanbanTaskLoadingMessage, true);

				remoteAction( $RemoteAction.KanbanController.getTask, [taskId], function ( result ) {
						KanbanController.showForm(result, KanbanController.saveTask);
						hideMsg();
					}
				);

				return false;
			});
		},

		/**
		 * @private
		 * Sends a request to save the provided task values on the database and
		 * handles the response.
		 * @param {TaskSummary} task A TaskSummary object to be saved.
		 */
		saveTask: function ( task ) {
			remoteAction( $RemoteAction.KanbanController.saveTask, [task], function ( result, event ) {
				// Reload all of the features to ensure they are all up to date
				KanbanController.loadFeatures();
			});
		},

		/**
		 * @private
		 * Loads the provided tasks data into the edit form.
		 * @param {TaskSummary} task The task data to load into the form
		 */
		setFormValues: function ( task ) {
			$j('#taskId').val( task.Id );
			$j('#taskTitle').val( task.Name );
			$j('#taskDesc').val( task.Description );
			$j('#taskEst').val( task.Estimate );
			$j('#taskRem').val( task.TotalHours );
			$j('#taskCategory').val( task.Category ).change();
			$j('#taskOwner').val( task.Assigned );
			$j('#taskStoryId').val( task.Feature );
			$j('#taskStatus').val( task.Status );
		},

		/**
		 * @private
		 * Collects all of the Task values from the form elements and returns a
		 * TaskSummary object.
		 * @return {TaskSummary} The TaskSummary object containing all of the form values
		 */
		getFormValues: function () {
			return {
				Id: $j('#taskId').val() || null,
				Name: $j('#taskTitle').val(),
				Description: $j('#taskDesc').val(),
				Estimate: $j('#taskEst').val(),
				TotalHours: $j('#taskRem').val(),
				Category: $j('#taskCategory').val(),
				Status: $j('#taskStatus').val(),
				Assigned: $j('#taskOwner').val() || null,
				Feature: $j('#taskStoryId').val()
			};
		},

		/**
		 * @private
		 * Shows the task edit form and hooks up an event listener to notify the
		 * caller when the user saves the changes. The onSave function will be
		 * provided with the form values as the first argument.
		 * @param {TaskSummary} task The task to edit with the form
		 * @param {Function} onSave A function to be called when the save button is pressed
		 */
		showForm: function ( task, onSave ) {
			KanbanController.setFormValues(task);

			$j("#editTask").dialog({
				title: $Label.KanbanEditTaskTitle + task.Name,
				height: 375,
				width: 600,
				modal: true,
				draggable: true,
				resizable: false,
				buttons: [{
			        text: 'Save',
			        click: function () {
						if ( onSave ) {
							onSave( KanbanController.getFormValues() );
						}

						$j(this).dialog("close");
					}
			    }, {
			        text: 'Cancel',
			        click: function() {
						$j(this).dialog('close');
					}
			    }]
			});
		}
	};

})();
