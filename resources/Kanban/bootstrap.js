(function () {

	KanbanController.loadProjects();

	$j('#taskCategory').change( function() {
		var category = $j(this).val().toLowerCase() || 'nocategory';

		$j('#categoryColour').removeClass().addClass( category );
	});

	$j('#projectList').change(function(){
		KanbanController.loadFeatures();
	});

	$j('#refreshBtn').click(function(){
		KanbanController.loadFeatures();
		return false;
	});

	$j('#helpBtn').click( function() {
		$j('#helpPage').dialog({
			title: $Label.KanbanHelpTitle,
			height: 420,
			width: 600,
			modal: true,
			draggable: true,
			resizable: false,
			open: function(event, ui) { $j(".ui-dialog-titlebar-close").show(); }
		});
	});

})();
