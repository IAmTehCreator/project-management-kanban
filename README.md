# Project Management - Kanban Extension

This repository contains an extension package for the [Project Management](https://bitbucket.org/IAmTehCreator/project-management) package for Salesforce. It has been forked from [John Conners](http://johnsadventures.com) Force.com Sprint Wall ([repository here](https://github.com/financialforcedev/ForceDotComSprintWall)) which was originally created for [FinancialForce](https://financialforce.com) to manage the stories and tasks undertaken within a sprint until [Jira](https://www.atlassian.com/software/jira) came along.

This package provides a dynamic sprint board which can allow features and bugs to be planned into sprints and broken down into sub-tasks which can be dragged around a kanban board to change status.

## Installation

This package is an extension of Project Management and requires that and it's dependencies to already be installed. If it is not installed then click the package names below to install them;

* [Utilities 2.1.0](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t58000000WL66)
* [Project Management 1.0.0](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t58000000MeEI)

Once the dependencies are installed click the link of the version you want to install below (scored out links are deprecated);

* [Project Management - Kanban 1.1.0](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t0Y000001maps)
* [Project Management - Kanban 1.0.0](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t0Y000001mahX)

## Building

To build this package you must have Apache Ant installed and a Salesforce org to deploy it to. Once you have cloned this repository you must create a file named `local.build.properties` in the root of the repository and copy and paste the following code into it;

```
# ==============================================================================
# =========================== Local Build Properties ===========================
# ==============================================================================
# Use this file to set build properties that will not get committed to source
# control as this file is gitignored. Comment a line out to recieve a default
# value from the build.properties file.

# ============================== Salesforce ====================================
# For a production org use: https://login.salesforce.com
# For a sandbox org use: https://test.salesforce.com
sf.serverurl = https://login.salesforce.com

sf.username = <Insert your Salesforce username here>
sf.password = <Insert your Salesforce password here>
sf.token = <Insert your Salesforce security token here>
```

You must then insert your Salesforce username, password and security token (if required) into the specified areas in the file. Once this has been done you can use Ant to deploy and undeploy the package from an org. Below is a list of Ant targets that this project supports;

* `ant deploy` - Deploys the package components to a Salesforce org
* `ant undeploy` - Deletes the package components from a Salesforce org
* `ant update.metadata` - Updates the `package.xml` manifest with all of the package components
* `ant retrieve.metadata` - Retrieves the package metadata from a Salesforce org and saves it locally

The build system also has some utility targets which can template Apex classes and VisualForce pages;

* `ant new.class` - Creates a new Apex Class and associated unit test class
* `ant new.visualforce` - Creates a new VisualForce page with associated Apex controller and unit test
* `ant new.lightning` - Creates a new Lightning Component Bundle with an Apex controller and unit test

## Licensing

This code has been released under the GNU General Public License. See the associated [license.txt](https://github.com/financialforcedev/ForceDotComSprintWall/blob/master/license.txt) file for more details.

## Included Libraries / Components

 - [jQuery](http://jquery.com)
 - [jQuery UI](http://jqueryui.com)
 - [jQuery Cookie Plugin](https://github.com/carhartl/jquery-cookie)
 - [jQuery Hotkeys Plugin](http://github.com/tzuryby/hotkeys)
 - [jQuery Sticky Table Headers Plugin](https://github.com/jmosbech/StickyTableHeaders)
 - [jQuery UI Touch Punch](http://touchpunch.furf.com/)
 - [famfamfam Silk Icons](http://www.famfamfam.com/lab/icons/silk/)
