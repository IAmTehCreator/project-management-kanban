<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:sf="com.salesforce" xmlns:ac="net.sf.antcontrib" xmlns:workspace="com.smilne.ant.workspace" name="undeploy-helper">

	<macrodef name="undeploy-basic-metadata" description="Undeploys metadata that does not contain dependencies" uri="com.salesforce">
		<attribute name="package" description="The package directory to undeploy" />
		<sequential>
			<workspace:new var="basic.undeploy" />

			<workspace:move workspace="${basic.undeploy}/triggers" dir="@{package}/triggers" />
			<workspace:move workspace="${basic.undeploy}/permissionsets" dir="@{package}/permissionsets" />
			<workspace:move workspace="${basic.undeploy}/profiles" dir="@{package}/profiles" />
			<workspace:move workspace="${basic.undeploy}/customMetadata" dir="@{package}/customMetadata" />
			<workspace:move workspace="${basic.undeploy}/dashboards" dir="@{package}/dashboards" />
			<workspace:move workspace="${basic.undeploy}/quickActions" dir="@{package}/quickActions" />
			<workspace:move workspace="${basic.undeploy}/homePageComponents" dir="@{package}/homePageComponents" />
			<workspace:move workspace="${basic.undeploy}/applications" dir="@{package}/applications" />
			<workspace:move workspace="${basic.undeploy}/tabs" dir="@{package}/tabs" />
			<workspace:move workspace="${basic.undeploy}/reports" dir="@{package}/reports" />

			<sf:package path="${basic.undeploy}/package.xml" />
			<sf:update-package output="${basic.undeploy}/destructiveChanges.xml" path="${basic.undeploy}" name="${sf.packageName}" description="${sf.description}" version="${sf.version}"/>

			<delete dir="${basic.undeploy}/triggers" />
			<delete dir="${basic.undeploy}/permissionsets" />
			<delete dir="${basic.undeploy}/profiles" />
			<delete dir="${basic.undeploy}/customMetadata" />
			<delete dir="${basic.undeploy}/dashboards" />
			<delete dir="${basic.undeploy}/quickActions" />
			<delete dir="${basic.undeploy}/homePageComponents" />
			<delete dir="${basic.undeploy}/applications" />
			<delete dir="${basic.undeploy}/tabs" />
			<delete dir="${basic.undeploy}/reports" />

			<echo>Undeploying simple metadata...</echo>
			<sf:deploy-dir dir="${basic.undeploy}" />

			<workspace:clean workspace="${basic.undeploy}" />
		</sequential>
	</macrodef>

	<macrodef name="stub-metadata" description="Stubs complex metadata types and deploys changes to the org" uri="com.salesforce">
		<attribute name="package" description="The package directory to undeploy" />
		<sequential>
			<workspace:new var="stubbing.deploy" />

			<!-- <workspace:import workspace="${stubbing.deploy}/classes" dir="@{package}/classes" /> -->
			<workspace:import workspace="${stubbing.deploy}/pages" dir="@{package}/pages" />
			<workspace:import workspace="${stubbing.deploy}/layouts" dir="@{package}/layouts" />

			<!-- <sf:stub-apex package="${stubbing.deploy}" /> -->
			<sf:stub-visualforce package="${stubbing.deploy}" />
			<sf:stub-layouts package="${stubbing.deploy}" />
			<sf:stub-fields package="@{package}" target="${stubbing.deploy}" />

			<sf:package path="${stubbing.deploy}/package.xml" version="39.0">
				<content>
					<!-- <sf:package-type type="ApexClass" dir="${stubbing.deploy}/classes" extension="cls" /> -->
					<sf:package-object-entity dir="${stubbing.deploy}/objects" type="CustomField" element="fields" />
					<sf:package-type type="ApexPage" dir="${stubbing.deploy}/pages" extension="page" />
					<sf:package-type type="Layout" dir="${stubbing.deploy}/layouts" extension="layout" />
				</content>
			</sf:package>

			<echo>Stubbing metadata...</echo>
			<sf:deploy-dir dir="${stubbing.deploy}" />

			<workspace:clean workspace="${stubbing.deploy}" />
		</sequential>
	</macrodef>

	<macrodef name="undeploy-bulk" description="Undeploys the bulk of the metadata" uri="com.salesforce">
		<attribute name="package" description="The package directory to undeploy" />
		<sequential>
			<sf:prepare-standard-undeploy package="@{package}" />		<!-- undeploy.standard -->
			<sf:prepare-visualforce-undeploy package="@{package}" />	<!-- undeploy.visualforce -->
			<sf:prepare-custom-undeploy package="@{package}" />			<!-- undeploy.custom -->
			<sf:prepare-sobject-undeploy package="@{package}" />		<!-- sobject.undeploy -->

			<echo>Undeploying declarative customisations...</echo>
			<sf:deploy-dir dir="${undeploy.custom}" />

			<echo>Undeploying customisations to standard objects...</echo>
			<sf:deploy-dir dir="${undeploy.standard}" />

			<echo>Undeploying SObjects...</echo>
			<sf:deploy-dir dir="${sobject.undeploy}" />

			<echo>Undeploying Apex...</echo>
			<sf:deploy-dir dir="${undeploy.visualforce}" />

			<workspace:clean workspace="${undeploy.custom}" />
			<workspace:clean workspace="${undeploy.standard}" />
			<workspace:clean workspace="${sobject.undeploy}" />
			<workspace:clean workspace="${undeploy.visualforce}" />
		</sequential>
	</macrodef>

	<macrodef name="prepare-standard-undeploy" description="Prepares a workspace with the standard object purge payload" uri="com.salesforce">
		<attribute name="package" description="The package directory to purge metadata from" />
		<sequential>
			<workspace:new var="undeploy.standard" />
			<workspace:import workspace="${undeploy.standard}/objects" dir="@{package}/objects" />
			<workspace:import workspace="${undeploy.standard}/layouts" dir="@{package}/layouts" />
			<delete><fileset dir="${undeploy.standard}/objects" includes="*__*" /></delete>
			<delete><fileset dir="${undeploy.standard}/layouts" includes="*__*" /></delete>

			<sf:package path="${undeploy.standard}/package.xml" />
			<sf:package path="${undeploy.standard}/destructiveChanges.xml">
				<content>
					<sf:package-type type="Layout" dir="${undeploy.standard}/layouts" extension="layout" />
					<sf:package-object-entity dir="${undeploy.standard}/objects" type="CustomField" element="fields" />
					<sf:package-object-entity dir="${undeploy.standard}/objects" type="ListView" element="listViews" />
					<sf:package-object-entity dir="${undeploy.standard}/objects" type="WebLink" element="webLinks" />
					<sf:package-object-entity dir="${undeploy.standard}/objects" type="CompactLayout" element="compactLayouts" />
				</content>
			</sf:package>
			<delete><fileset dir="${undeploy.standard}" includes="**/*" excludes="package.xml,destructiveChanges.xml" /></delete>

			<delete><fileset dir="@{package}/objects" includes="*.*" excludes="*__*" /></delete>
			<delete><fileset dir="@{package}/layouts" includes="*.*" excludes="*__*" /></delete>
		</sequential>
	</macrodef>

	<macrodef name="prepare-visualforce-undeploy" description="Prepares a workspace with the VisualForce purge payload" uri="com.salesforce">
		<attribute name="package" description="The package directory to purge metadata from" />
		<sequential>
			<workspace:new var="undeploy.visualforce" />
			<workspace:move workspace="${undeploy.visualforce}/pages" dir="@{package}/pages" />

			<sf:package path="${undeploy.visualforce}/package.xml" />
			<sf:package path="${undeploy.visualforce}/destructiveChanges.xml">
				<content>
					<sf:package-type type="ApexPage" dir="${undeploy.visualforce}/pages" extension="page" />
				</content>
			</sf:package>
			<delete><fileset dir="${undeploy.visualforce}" includes="**/*" excludes="package.xml,destructiveChanges.xml" /></delete>
		</sequential>
	</macrodef>

	<macrodef name="prepare-custom-undeploy" description="Prepares a workspace with the custom metadata purge payload" uri="com.salesforce">
		<attribute name="package" description="The package directory to purge metadata from" />
		<sequential>
			<workspace:new var="undeploy.custom" />
			<sf:package path="${undeploy.custom}/package.xml" />
			<sf:package path="${undeploy.custom}/destructiveChanges.xml">
				<content>
					<sf:package-aura dir="@{package}/aura" />
					<sf:package-type type="ApexClass" dir="@{package}/classes" extension="cls" />
					<sf:package-type type="ApexComponent" dir="@{package}/components" extension="component" />
					<sf:package-type type="CustomMetadata" dir="@{package}/customMetadata" extension="md" />
					<sf:package-type type="CustomPermission" dir="@{package}/customPermissions" extension="customPermission" />
					<sf:package-labels file="@{package}/labels/CustomLabels.labels" />
					<sf:package-type type="StaticResource" dir="@{package}/staticresources" extension="resource" />
				</content>
			</sf:package>
		</sequential>
	</macrodef>

	<macrodef name="prepare-sobject-undeploy" description="Prepares a workspace with the SObject purge payload" uri="com.salesforce">
		<attribute name="package" description="The package directory to purge metadata from" />
		<sequential>
			<workspace:new var="sobject.undeploy" />
			<sf:package path="${sobject.undeploy}/package.xml" />
			<sf:package path="${sobject.undeploy}/destructiveChanges.xml">
				<content>
					<sf:package-name name="${sf.packageName}" />
					<sf:package-type type="CustomObject" dir="@{package}/objects" extension="object" />
				</content>
			</sf:package>
		</sequential>
	</macrodef>

</project>
