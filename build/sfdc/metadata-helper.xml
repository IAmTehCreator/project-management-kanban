<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:sf="com.salesforce" xmlns:ac="net.sf.antcontrib" xmlns:sobject="com.salesforce.schema" name="metadata-helper">

	<!-- SObject Metadata Helpers -->

	<macrodef name="each-object" description="Itterates over all object files in the given folder" uri="com.salesforce">
		<attribute name="dir" description="The objects directory to scan for fields" />
		<element name="object-do" description="The block of tasks to perform on each object" />
		<sequential>
			<ac:for param="object.file">
				<fileset dir="@{dir}">
					<include name="**/*.object" />
				</fileset>
				<sequential>
					<ac:propertyregex property="object.name" input="@{object.file}" regexp="(.+)\/(.+)\." select="\2" override="true" />

					<ac:var name="object" value="${object.name}" />
					<ac:var name="file" value="@{object.file}" />
					<object-do />
				</sequential>
			</ac:for>
		</sequential>
	</macrodef>

	<macrodef name="each-sobject-field" description="Itterates over all SObject fields in the given package" uri="com.salesforce">
		<attribute name="file" description="The SObject metadata file name" />
		<element name="field-do" description="The block of tasks to perform on each object" />
		<sequential>
			<ac:var name="file.contents" unset="true" />
			<loadfile property="file.contents" srcFile="@{file}" />

			<ac:var name="field.definition" value="" />
			<ac:var name="is.field" value="false" />
			<ac:for param="line" list="${file.contents}" delimiter="&#xA;">
				<sequential>
					<ac:if><contains string="@{line}" substring="&lt;fields&gt;" />
						<then><ac:var name="is.field" value="true" /></then>
						<else>
							<ac:if><contains string="@{line}" substring="&lt;/fields&gt;" />
								<then>
									<ac:propertyregex property="field.name" override="true" regexp="&lt;fullName&gt;(.+)&lt;\/fullName&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.type" override="true" regexp="&lt;type&gt;(.+)&lt;\/type&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.label" override="true" regexp="&lt;label&gt;(.+)&lt;\/label&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.referenceTo" override="true" regexp="&lt;referenceTo&gt;(.+)&lt;\/referenceTo&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.relationshipLabel" override="true" regexp="&lt;relationshipLabel&gt;(.+)&lt;\/relationshipLabel&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.relationshipName" override="true" regexp="&lt;relationshipName&gt;(.+)&lt;\/relationshipName&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.deleteConstraint" override="true" regexp="&lt;deleteConstraint&gt;(.+)&lt;\/deleteConstraint&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.formula" override="true" regexp="&lt;formula&gt;(.+)&lt;\/formula&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.precision" override="true" regexp="&lt;precision&gt;(.+)&lt;\/precision&gt;" input="${field.definition}" select="\1" />
									<ac:propertyregex property="field.scale" override="true" regexp="&lt;scale&gt;(.+)&lt;\/scale&gt;" input="${field.definition}" select="\1" />

									<field-do />

									<ac:var name="field.definition" value="" />
									<ac:var name="is.field" value="false" />
								</then>
							</ac:if>

							<ac:if><equals arg1="${is.field}" arg2="true" />
								<then><ac:var name="field.definition" value="${field.definition}@{line}&#xA;" /></then>
							</ac:if>
						</else>
					</ac:if>
				</sequential>
			</ac:for>
		</sequential>
	</macrodef>

	<!-- Stubbing -->

	<macrodef name="stub-apex" description="Stubs all Apex classes in the provided package directory" uri="com.salesforce">
		<attribute name="package" description="The path to the package directory where classes will be stubbed" />
		<sequential>
			<ac:if><available file="@{package}/classes" type="dir" />
				<then>
					<delete>
						<fileset dir="@{package}/classes" includes="*Exception*" />
					</delete>
					<ac:for param="file">
						<path>
							<fileset dir="@{package}/classes" includes="*.cls" />
						</path>
						<sequential>
							<ac:propertyregex property="name" input="@{file}" regexp="(.+)\/(.+)\." select="\2" override="true" />
							<sf:template-class-global file="@{file}" classname="${name}" />
						</sequential>
					</ac:for>
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="stub-visualforce" description="Stubs all VisualForce pages in the provided package directory" uri="com.salesforce">
		<attribute name="package" description="The path to the package directory where pages will be stubbed" />
		<sequential>
			<ac:if><available file="@{package}/pages" type="dir" />
				<then>
					<ac:for param="file">
						<path>
							<fileset dir="@{package}/pages" includes="*.page" />
						</path>
						<sequential>
							<ac:propertyregex property="name" input="@{file}" regexp="(.+)\/(.+)\." select="\2" override="true" />
							<sf:template-page file="@{file}" name="${name}" label="${name}" />
						</sequential>
					</ac:for>
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="stub-layouts" description="Stubs all SObject layouts in the provided package directory" uri="com.salesforce">
		<attribute name="package" description="The path to the package directory where layouts will be stubbed" />
		<sequential>
			<ac:if><available file="@{package}/layouts" type="dir" />
				<then>
					<ac:for param="file">
						<path>
							<fileset dir="@{package}/layouts" includes="*.layout" />
						</path>
						<sequential>
							<ac:if><contains string="@{file}" substring="__mdt" />
								<then>
									<sf:template-custom-metadata-layout file="@{file}" />
								</then>
								<else>
									<ac:var name="file.contents" unset="true" />
									<loadfile property="file.contents" srcFile="@{file}" />

									<ac:var name="last.line" value="" />
									<ac:for param="line" list="${file.contents}" delimiter="&#xA;">
										<sequential>
											<ac:if><contains string="@{line}" substring="&lt;field&gt;Name&lt;/field&gt;" />
												<then>
													<ac:propertyregex property="behavior" override="true" regexp="&lt;behavior&gt;(.+)&lt;\/behavior&gt;" input="${last.line}" select="\1" />
												</then>
											</ac:if>
											<ac:var name="last.line" value="@{line}" />
										</sequential>
									</ac:for>

									<sf:template-layout file="@{file}" behavior="${behavior}" />
									<ac:var name="behavior" unset="true" />
								</else>
							</ac:if>
						</sequential>
					</ac:for>
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="stub-fields" description="Stubs SObject fields so they cannot reference other fields" uri="com.salesforce">
		<attribute name="package" description="The path to the package directory where fields will be stubbed" />
		<attribute name="target" description="The directory to write the stubs to" />
		<sequential>
			<sf:each-object dir="@{package}">
				<object-do>
					<sobject:new package="@{target}" name="${object}" defaults="false">
						<content>
							<sf:each-sobject-field file="${file}">
								<field-do>
									<sf:is-formula definition="${field.definition}">
										<do>
											<sobject:field name="${field.name}" label="${field.label}" type="${field.type}">
												<field-content>
													<sobject:field-formula code="NULL" />

													<!-- Copy existing properties from field -->
													<sobject:property-field />
												</field-content>
											</sobject:field>
										</do>
									</sf:is-formula>

									<ac:if><equals arg1="${field.type}" arg2="Lookup" />
										<then>
											<sobject:field name="${field.name}" label="${field.label}" type="${field.type}">
												<field-content>
													<sobject:field-lookup name="${field.relationshipName}" target="${field.referenceTo}" label="${field.relationshipLabel}" />
												</field-content>
											</sobject:field>
										</then>
									</ac:if>
								</field-do>
							</sf:each-sobject-field>

							<!-- Stub the search layout while we're at it -->
							<sobject:empty-element name="searchLayouts" />
						</content>
					</sobject:new>
				</object-do>
			</sf:each-object>
		</sequential>
	</macrodef>

	<macrodef name="is-formula" description="Takes a field definition and calls the do task with a formula definition if the field is a formula" uri="com.salesforce">
		<attribute name="definition" description="The field definition" />
		<element name="do" description="A node containing tasks to execute if the field is a formula" />
		<sequential>
			<ac:if><contains string="@{definition}" substring="&lt;formula&gt;" />
				<then>
					<do />
				</then>
			</ac:if>
		</sequential>
	</macrodef>

</project>
