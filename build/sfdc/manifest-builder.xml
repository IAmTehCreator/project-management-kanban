<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:sf="com.salesforce" xmlns:ac="net.sf.antcontrib" xmlns:workspace="com.smilne.ant.workspace" name="manifest-builder">

	<macrodef name="package" description="Dynamically creates a package.xml file" uri="com.salesforce">
		<attribute name="path" description="The path including filename of the file to be created" />
		<attribute name="version" default="${sf.default.version}" description="The API version the package should use" />
		<element name="content" optional="true" description="Container for 'package-line' and 'package-type' elements" />
		<sequential>
			<echo file="@{path}"><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<Package xmlns="http://soap.sforce.com/2006/04/metadata">
]]></echo>

			<ac:var name="package.path" value="@{path}" />
			<content />

			<echo file="@{path}" append="true"><![CDATA[	<version>@{version}</version>
</Package>]]></echo>
		</sequential>
	</macrodef>

	<macrodef name="package-line" description="To be used within 'sf:package' to add a line to the XML file" uri="com.salesforce">
		<attribute name="content" description="The text to insert into the package" />
		<sequential>
			<echo file="${package.path}" append="true">&#009;@{content}&#xA;</echo>
		</sequential>
	</macrodef>

	<macrodef name="package-name" description="To be used within 'sf:package' to add a package name" uri="com.salesforce">
		<attribute name="name" description="The name of the package" />
		<sequential>
			<sf:package-line content="&lt;fullName&gt;@{name}&lt;/fullName&gt;" />
		</sequential>
	</macrodef>

	<macrodef name="package-description" description="To be used within 'sf:package' to add a package description" uri="com.salesforce">
		<attribute name="description" description="The description of the package" />
		<sequential>
			<sf:package-line content="&lt;description&gt;@{description}&lt;/description&gt;" />
		</sequential>
	</macrodef>

	<macrodef name="package-access" description="To be used within 'sf:package' to set the package API access level" uri="com.salesforce">
		<attribute name="level" description="The API access level the package has" />
		<sequential>
			<sf:package-line content="&lt;apiAccessLevel&gt;@{level}&lt;/apiAccessLevel&gt;" />
		</sequential>
	</macrodef>

	<macrodef name="package-namespace" description="To be used within 'sf:package' to set the package namespace" uri="com.salesforce">
		<attribute name="prefix" description="The package namespace prefix" />
		<sequential>
			<sf:package-line content="&lt;namespacePrefix&gt;@{prefix}&lt;/namespacePrefix&gt;" />
		</sequential>
	</macrodef>

	<macrodef name="package-type" description="To be used within 'sf:package' to add a metadata type to the package" uri="com.salesforce">
		<attribute name="type" description="Name of the metadata type" />
		<attribute name="members" default="" description="Comma separated list of members of the metadata type" />
		<attribute name="dir" default="" description="The directory to read the type members from" />
		<attribute name="extension" default="" description="The filename extension of members" />
		<sequential>
			<ac:var name="member.list" value="@{members}" />

			<ac:if>
				<and>
					<not><equals arg1="@{dir}" arg2="" /></not>
					<available file="@{dir}" type="dir" />
				</and>
				<then>
					<ac:for param="file">
						<path>
							<fileset dir="@{dir}" includes="**/*.@{extension}" />
						</path>
						<sequential>
							<ac:propertyregex property="entry" input="@{file}" regexp="(.+)\/(.+)\." select="\2" override="true" />

							<ac:var name="member.list" value="${member.list},${entry}" />
						</sequential>
					</ac:for>
				</then>
			</ac:if>

			<ac:if><not><equals arg1="${member.list}" arg2="" /></not>
				<then>
					<ac:var name="member.xml" value="" />
					<ac:for list="${member.list}" param="member.name">
						<sequential>
							<ac:var name="member.xml" value="${member.xml}&#xA;&#009;&#009;&lt;members&gt;@{member.name}&lt;/members&gt;" />
						</sequential>
					</ac:for>

					<sf:package-line content="&lt;types&gt;${member.xml}&#xA;&#009;&#009;&lt;name&gt;@{type}&lt;/name&gt;&#xA;&#009;&lt;/types&gt;" />
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="package-labels" description="Includes lables in the package in 'sf:package'" uri="com.salesforce">
		<attribute name="file" description="The custom label file to itterate over" />
		<sequential>
			<ac:if><available file="@{file}" />
				<then>
					<ac:var name="label.list" value="" />
					<loadfile property="label.file" srcFile="@{file}" />
					<ac:for param="line" list="${label.file}" delimiter="&#xA;">
						<sequential>
							<ac:if><contains string="@{line}" substring="&lt;fullName&gt;" />
								<then>
									<ac:propertyregex property="label.name" override="true" regexp="&lt;fullName&gt;(.+)&lt;\/fullName&gt;" input="@{line}" select="\1" />

									<ac:var name="label.list" value="${label.list},${label.name}" />
								</then>
							</ac:if>
						</sequential>
					</ac:for>

					<sf:package-type type="CustomLabel" members="${label.list}" />
					<sf:package-type type="CustomLabels" members="CustomLabels" />
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="package-object-entity" description="Packages a metadata type defined within object metadata files" uri="com.salesforce">
		<attribute name="dir" description="The objects directory to scan" />
		<attribute name="type" description="The metadata type to generate package.xml entries for" />
		<attribute name="element" description="The element to search the object file for" />
		<element name="members" description="" />
		<sequential>
			<ac:if><available file="@{dir}" type="dir" />
				<then>
					<workspace:new var="object.entities" />

					<sf:each-object dir="@{dir}">
						<object-do>
							<ac:var name="output.file" value="${object.entities}/object-@{type}.members" />
							<sf:invoke-package-member target="@{element}" prefix="${object}." output="${output.file}" />
						</object-do>
					</sf:each-object>

					<ac:if><available file="${output.file}" />
						<then>
							<echo file="${package.path}" append="true">&#009;&lt;types&gt;&#xA;</echo>

							<concat destfile="${package.path}" append="true">
								<file file="${output.file}" />
							</concat>

							<echo file="${package.path}" append="true">&#009;&#009;&lt;name&gt;@{type}&lt;/name&gt;&#xA;</echo>
							<echo file="${package.path}" append="true">&#009;&lt;/types&gt;&#xA;</echo>
						</then>
					</ac:if>

					<workspace:clean workspace="${object.entities}" />
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="invoke-package-member" description="Utility macro used within 'sf:each-object'" uri="com.salesforce">
		<attribute name="target" description="The target member container node" />
		<attribute name="prefix" default="" description="A prefix to add to member names" />
		<attribute name="output" default="" description="Optional. The file to write the contents to" />
		<sequential>
			<ac:var name="output.file" value="${package.path}" />
			<ac:if><not><equals arg1="@{output}" arg2="" /></not>
				<then><ac:var name="output.file" value="@{output}" /></then>
			</ac:if>

			<ant target="sf-package-object-member">
				<property name="package" value="${output.file}" />
				<property name="object" value="${object}" />
				<property name="file" value="${file}" />
				<property name="target" value="@{target}" />
				<property name="prefix" value="@{prefix}" />
			</ant>
		</sequential>
	</macrodef>

	<target name="sf-package-object-member" description="Utility target for adding object members to the package.xml">
		<loadfile property="object.content" srcFile="${file}" />
		<echo>Searching ${object} for ${target}...</echo>

		<ac:var name="is.target" value="false" />
		<ac:var name="is.picklist" value="false" />
		<ac:var name="is.picklistValues" value="false" />
		<ac:for param="line" list="${object.content}" delimiter="&#xA;">
			<sequential>
				<ac:if><contains string="@{line}" substring="&lt;${target}&gt;" />
					<then><ac:var name="is.target" value="true" /></then>
				</ac:if>
				<ac:if><contains string="@{line}" substring="&lt;/${target}&gt;" />
					<then><ac:var name="is.target" value="false" /></then>
				</ac:if>
				<ac:if><contains string="@{line}" substring="&lt;picklist&gt;" />
					<then><ac:var name="is.picklist" value="true" /></then>
				</ac:if>
				<ac:if><contains string="@{line}" substring="&lt;/picklist&gt;" />
					<then><ac:var name="is.picklist" value="false" /></then>
				</ac:if>
				<ac:if><contains string="@{line}" substring="&lt;picklistValues&gt;" />
					<then><ac:var name="is.picklistValues" value="true" /></then>
				</ac:if>
				<ac:if><contains string="@{line}" substring="&lt;/picklistValues&gt;" />
					<then><ac:var name="is.picklistValues" value="false" /></then>
				</ac:if>
				<ac:if>
					<and>
						<equals arg1="${is.target}" arg2="true" />
						<equals arg1="${is.picklist}" arg2="false" />
						<equals arg1="${is.picklistValues}" arg2="false" />
						<contains string="@{line}" substring="&lt;fullName&gt;" />
					</and>
					<then>
						<ac:propertyregex property="member.name" override="true" regexp="&lt;fullName&gt;(.+)&lt;\/fullName&gt;" input="@{line}" select="\1" />

						<echo>Found: ${prefix}${member.name}</echo>
						<echo file="${package}" append="true">&#009;&#009;&lt;members&gt;${prefix}${member.name}&lt;/members&gt;&#xA;</echo>
					</then>
				</ac:if>
			</sequential>
		</ac:for>
	</target>

	<macrodef name="package-reports" description="" uri="com.salesforce">
		<attribute name="dir" description="The reports directory" />
		<sequential>
			<ac:if><available file="@{dir}" type="dir" />
				<then>
					<ac:var name="report.members" value="" />

					<ac:for param="file">
						<path><fileset dir="@{dir}" includes="**/*-meta.xml" /></path>
						<sequential>
							<ac:propertyregex property="entry" input="@{file}" regexp="(.+)\/(.+)\-meta\.xml" select="\2" override="true" />

							<ac:var name="report.members" value="${report.members},${entry}" />
						</sequential>
					</ac:for>

					<ac:for param="file">
						<path><fileset dir="@{dir}" includes="**/*.report" /></path>
						<sequential>
							<ac:propertyregex property="parent" input="@{file}" regexp=".+\/(.+)\/(.+)\." select="\1" override="true" />
							<ac:propertyregex property="entry" input="@{file}" regexp="(.+)\/(.+)\." select="\2" override="true" />

							<ac:var name="report.members" value="${report.members},${parent}/${entry}" />
						</sequential>
					</ac:for>

					<ac:if><not><equals arg1="${report.members}" arg2="" /></not>
						<then>
							<sf:package-type type="Report" members="${report.members}" />
						</then>
					</ac:if>
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="package-dashboards" description="" uri="com.salesforce">
		<attribute name="dir" description="The dashboards directory" />
		<sequential>
			<ac:if><available file="@{dir}" type="dir" />
				<then>
					<ac:var name="dashboard.members" value="" />

					<ac:for param="file">
						<path><fileset dir="@{dir}" includes="**/*-meta.xml" /></path>
						<sequential>
							<ac:propertyregex property="entry" input="@{file}" regexp="(.+)\/(.+)\-meta\.xml" select="\2" override="true" />

							<ac:var name="dashboard.members" value="${dashboard.members},${entry}" />
						</sequential>
					</ac:for>

					<ac:for param="file">
						<path><fileset dir="@{dir}" includes="**/*.dashboard" /></path>
						<sequential>
							<ac:propertyregex property="parent" input="@{file}" regexp=".+\/(.+)\/(.+)\." select="\1" override="true" />
							<ac:propertyregex property="entry" input="@{file}" regexp="(.+)\/(.+)\." select="\2" override="true" />

							<ac:var name="dashboard.members" value="${dashboard.members},${parent}/${entry}" />
						</sequential>
					</ac:for>

					<ac:if><not><equals arg1="${dashboard.members}" arg2="" /></not>
						<then>
							<sf:package-type type="Dashboard" members="${dashboard.members}" />
						</then>
					</ac:if>
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="package-aura" description="" uri="com.salesforce">
		<attribute name="dir" description="The reports directory" />
		<sequential>
			<ac:if><available file="@{dir}" type="dir" />
				<then>
					<ac:var name="aura.members" value="" />

					<ac:for param="file">
						<path>
							<dirset dir="@{dir}/" includes="*/**" />
						</path>
						<sequential>
							<ac:propertyregex property="entry" input="@{file}" regexp="(.+)\/(.+)" select="\2" override="true" />

							<ac:var name="aura.members" value="${aura.members},${entry}" />
						</sequential>
					</ac:for>

					<ac:if><not><equals arg1="${aura.members}" arg2="" /></not>
						<then>
							<sf:package-type type="AuraDefinitionBundle" members="${aura.members}" />
						</then>
					</ac:if>
				</then>
			</ac:if>
		</sequential>
	</macrodef>

	<macrodef name="package-types-all" description="Includes all package types in 'sf:package'" uri="com.salesforce">
		<sequential>
			<sf:package-type type="ApexClass" members="*" />
			<sf:package-type type="ApexComponent" members="*" />
			<sf:package-type type="ApexPage" members="*" />
			<sf:package-type type="ApexTrigger" members="*" />
			<sf:package-type type="AuraDefinitionBundle" members="*" />
			<sf:package-type type="CompactLayout" members="*" />
			<sf:package-type type="CustomLabels" members="*" />
			<sf:package-type type="CustomApplication" members="*" />
			<sf:package-type type="CustomField" members="*" />
			<sf:package-type type="CustomMetadata" members="*" />
			<sf:package-type type="CustomObject" members="*" />
			<sf:package-type type="CustomPermission" members="*" />
			<sf:package-type type="CustomTab" members="*" />
			<sf:package-type type="Dashboard" members="*" />
			<sf:package-type type="FlexiPage" members="*" />
			<sf:package-type type="GlobalPicklist" members="*" />
			<sf:package-type type="HomePageComponent" members="*" />
			<sf:package-type type="Layout" members="*" />
			<sf:package-type type="ListView" members="*" />
			<sf:package-type type="PermissionSet" members="*" />
			<sf:package-type type="Profile" members="*" />
			<sf:package-type type="QuickAction" members="*" />
			<sf:package-type type="RecordType" members="*" />
			<sf:package-type type="RemoteSiteSetting" members="*" />
			<sf:package-type type="Report" members="*" />
			<sf:package-type type="StaticResource" members="*" />
			<sf:package-type type="WebLink" members="" />
		</sequential>
	</macrodef>

	<macrodef name="update-package" description="Updates a package.xml" uri="com.salesforce">
		<attribute name="name" description="The name of the package" />
		<attribute name="description" description="The description of the package" />
		<attribute name="path" description="The path to the package.xml file to update" />
		<attribute name="version" default="${sf.default.version}" description="The Salesforce API version to target" />
		<attribute name="output" default="" description="The path to the output package.xml" />
		<sequential>
			<ac:var name="package.xml" value="@{output}" />
			<ac:if><equals arg1="@{output}" arg2="" />
				<then>
					<ac:var name="package.xml" value="@{path}/package.xml" />
				</then>
			</ac:if>

			<sf:package path="${package.xml}" version="@{version}">
				<content>
					<sf:package-name name="@{name}" />
					<sf:package-description description="@{description}" />

					<antcall target="on.update.package" />

					<sf:package-type type="ApexClass" dir="@{path}/classes" extension="cls" />
					<sf:package-type type="ApexComponent" dir="@{path}/components" extension="component" />
					<sf:package-type type="ApexPage" dir="@{path}/pages" extension="page" />
					<sf:package-type type="ApexTrigger" dir="@{path}/triggers" extension="trigger" />
					<sf:package-aura dir="@{path}/aura" />
					<sf:package-object-entity dir="@{path}/objects" type="CompactLayout" element="compactLayouts" />
					<sf:package-type type="CustomApplication" dir="@{path}/applications" extension="app" />
					<sf:package-object-entity dir="@{path}/objects" type="CustomField" element="fields" />
					<sf:package-type type="CustomMetadata" dir="@{path}/customMetadata" extension="md" />
					<sf:package-type type="CustomObject" dir="@{path}/objects" extension="object" />
					<sf:package-type type="CustomPermission" dir="@{path}/customPermissions" extension="customPermission" />
					<sf:package-type type="CustomTab" dir="@{path}/tabs" extension="tab" />
					<sf:package-dashboards dir="@{path}/dashboards" />
					<sf:package-type type="FlexiPage" dir="@{path}/flexipages" extension="flexipage" />
					<sf:package-type type="GlobalPicklist" dir="@{path}/globalPicklists" extension="globalPicklist" />
					<sf:package-type type="HomePageComponent" dir="@{path}/homePageComponents" extension="homePageComponent" />
					<sf:package-labels file="@{path}/labels/CustomLabels.labels" />
					<sf:package-type type="Layout" dir="@{path}/layouts" extension="layout" />
					<sf:package-object-entity dir="@{path}/objects" type="ListView" element="listViews" />
					<sf:package-type type="PermissionSet" dir="@{path}/permissionsets" extension="permissionset" />
					<sf:package-type type="QuickAction" dir="@{path}/quickActions" extension="quickAction" />
					<sf:package-type type="Profile" dir="@{path}/profiles" extension="profile" />
					<sf:package-type type="StaticResource" dir="@{path}/staticresources" extension="resource" />
					<sf:package-object-entity dir="@{path}/objects" type="RecordType" element="recordTypes" />
					<sf:package-type type="RemoteSiteSetting" dir="@{path}/remoteSiteSettings" extension="remoteSite" />
					<sf:package-reports dir="@{path}/reports" />
					<sf:package-object-entity dir="@{path}/objects" type="WebLink" element="webLinks" />
				</content>
			</sf:package>
		</sequential>
	</macrodef>

</project>
