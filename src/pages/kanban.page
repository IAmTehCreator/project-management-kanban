<apex:page controller="KanbanController" sidebar="false" tabStyle="Kanban__tab">

	<c:jQuery version="1.7.1" ui="1.8.16" theme="ui-lightness" var="$j" plugins="touch-punch, hotkeys, sticky-table-headers, cookie" />

	<apex:stylesheet value="{!URLFOR($Resource.Kanban, 'sprintwall.css')}" />
	<apex:includeScript value="{!URLFOR($Resource.Kanban, 'renderer.js')}" loadOnReady="true" />
	<apex:includeScript value="{!URLFOR($Resource.Kanban, 'controller.js')}" loadOnReady="true" />
	<apex:includeScript value="{!URLFOR($Resource.Kanban, 'bootstrap.js')}" loadOnReady="true" />

	<script type="text/javascript">
		var $RemoteAction = {};
		var $Params = {};
		var $Label = {};

		$RemoteAction.KanbanController = {};
		$RemoteAction.KanbanController.getProjects = "{!$RemoteAction.KanbanController.getProjects}";
		$RemoteAction.KanbanController.getFeatures = "{!$RemoteAction.KanbanController.getFeatures}";
		$RemoteAction.KanbanController.updateTaskStatus = "{!$RemoteAction.KanbanController.updateTaskStatus}";
		$RemoteAction.KanbanController.getTask = "{!$RemoteAction.KanbanController.getTask}";
		$RemoteAction.KanbanController.saveTask = "{!$RemoteAction.KanbanController.saveTask}";
		$RemoteAction.KanbanController.getUsers = "{!$RemoteAction.KanbanController.getUsers}";
		$RemoteAction.KanbanController.startFeature = "{!$RemoteAction.KanbanController.startFeature}";
		$RemoteAction.KanbanController.completeFeature = "{!$RemoteAction.KanbanController.completeFeature}";

		$Params.ArrowOutImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'arrow_out.png') )}";
		$Params.ArrowInImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'arrow_in.png') )}";
		$Params.ZoomImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'zoom.png') )}";
		$Params.AddImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'add.png') )}";
		$Params.UserAddImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'user_add.png') )}";
		$Params.CogImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'cog.png') )}";
		$Params.StartImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'start.png') )}";
		$Params.CompleteImageUrl = "{!JSENCODE( URLFOR($Resource.Kanban, 'complete.png') )}";

		$Label.Loading = "{!JSENCODE( $Label.Loading )}";
		$Label.KanbanProjectLoadingMessage = "{!JSENCODE( $Label.KanbanProjectLoadingMessage )}";
		$Label.KanbanTaskLoadingMessage = "{!JSENCODE( $Label.KanbanTaskLoadingMessage )}";
		$Label.KanbanHelpTitle = "{!JSENCODE( $Label.KanbanHelpTitle )}";
		$Label.KanbanErrorTitle = "{!JSENCODE( $Label.KanbanErrorTitle )}";
		$Label.KanbanEditTaskTitle = "{!JSENCODE( $Label.KanbanEditTaskTitle )}";
		$Label.KanbanNoTasksToDisplay = "{!JSENCODE( $Label.KanbanNoTasksToDisplay )}";
	</script>

	<apex:sectionHeader title="{!$Label.KanbanTitle}" />

	<div id="db" style="display: none"></div>
	<div id="error"></div>
	<div id="msg"></div>

	<div id="editTask" style="display: none">
		<table class="detailList" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td class="labelCol">
						<label for="taskTitle">{!HTMLENCODE($Label.KanbanTitleField)}</label>
					</td>
					<td class="dataCol">
						<input id="taskTitle" maxlength="80" name="taskTitle" size="20" type="text" value="" />
					</td>
				</tr>
				<tr>
					<td class="labelCol">
						<label for="taskDesc">{!HTMLENCODE($Label.KanbanDescriptionField)}</label>
					</td>
					<td class="dataCol">
						<textarea id="taskDesc" name="taskDesc" cols="80" rows="5"></textarea>
					</td>
				</tr>
				<tr>
					<td class="labelCol">
						<label for="taskEst">{!HTMLENCODE($Label.KanbanEstimateField)}</label>
					</td>
					<td class="dataCol">
						<input id="taskEst" maxlength="80" name="taskEst" size="20" type="text" value="" />
					</td>
				</tr>
				<tr>
					<td class="labelCol">
						<label for="taskRem">{!HTMLENCODE($Label.KanbanTotalHoursField)}</label>
					</td>
					<td class="dataCol">
						<input id="taskRem" maxlength="80" name="taskRem" size="20" type="text" value="" />
					</td>
				</tr>
				<tr>
					<td class="labelCol">
						<label for="taskCategory">{!HTMLENCODE($Label.KanbanCategoryField)}</label>
					</td>
					<td class="dataCol">
						<select id="taskCategory" name="taskCategory">
							<option value="">-- None --</option>
							<option value="Analysis">Analysis</option>
							<option value="Design">Design</option>
							<option value="Development">Development</option>
							<option value="QA">QA</option>
							<option value="Documentation">Documentation</option>
							<option value="Bug">Bug</option>
						</select>
						&nbsp;
						<span id="categoryColour" class="nocategory">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					</td>
				</tr>
				<tr>
					<td class="labelCol">
						<label for="taskOwner">{!HTMLENCODE($Label.KanbanOwnerField)}</label>
					</td>
					<td class="dataCol">
						<select id="taskOwner" name="taskOwner">
							<option value="">-- None --</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" id="taskId" value=""></input>
		<input type="hidden" id="taskStatus" value=""></input>
		<input type="hidden" id="taskStoryId" value=""></input>
	</div>

	<div id="helpPage" style="display: none">
		<p>{!HTMLENCODE($Label.KanbanHelpParagraph1)}</p>
		<p>{!HTMLENCODE($Label.KanbanHelpParagraph2)}</p>
		<p>{!HTMLENCODE($Label.KanbanHelpParagraph3)}</p>

		<table>
			<tr>
				<td class="nocategory">&nbsp;</td>
				<td>No category</td>
			</tr>
			<tr>
				<td class="analysis">&nbsp;</td>
				<td>Analysis</td>
			</tr>
			<tr>
				<td class="development">&nbsp;</td>
				<td>Development</td>
			</tr>
			<tr>
				<td class="design">&nbsp;</td>
				<td>Design</td>
			</tr>
			<tr>
				<td class="qa">&nbsp;</td>
				<td>QA</td>
			</tr>
			<tr>
				<td class="documentation">&nbsp;</td>
				<td>Documentation</td>
			</tr>
			<tr>
				<td class="bug">&nbsp;</td>
				<td>Bug</td>
			</tr>
		</table>

		<p>
			{!HTMLENCODE($Label.KanbanHappyCoding)}
			<apex:image value="{!URLFOR($Resource.Kanban, 'emoticon_smile.png')}" alt=":)" />
		</p>

		<div>Icons made by <a href="https://www.flaticon.com/authors/vaadin" title="Vaadin">Vaadin</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
		<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
	</div>

	<div id="completeConfirm" title="Complete Feature" style="display: none">
		<p>
			<span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
			{!HTMLENCODE($Label.KanbanCompleteFeatureDialog)}
		</p>
	</div>

	<div id="startConfirm" title="Start Feature" style="display: none">
		<p>
			<span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
			{!HTMLENCODE($Label.KanbanStartFeatureDialog)}
		</p>
	</div>

	<span>
		<label for="projectList">{!HTMLENCODE($Label.KanbanProjectField)}</label>
		<select id="projectList">
			<option value="">{!HTMLENCODE($Label.Loading)}</option>
		</select>
	</span>

	<a id="refreshBtn" href="#" title="{!$Label.Refresh}">
		<apex:image value="{!URLFOR($Resource.Kanban, 'arrow_rotate_clockwise.png')}" alt="" />
	</a>

	<span id="helpBtn">
		<a href="#" title="{!$Label.KanbanHelpMe}">
			<apex:image value="{!URLFOR($Resource.Kanban, 'help.png')}" alt="" />
		</a>
	</span>

	<div id="main"></div>

</apex:page>
