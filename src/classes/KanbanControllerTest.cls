@isTest(SeeAllData=false)
public class KanbanControllerTest
{
	@isTest
	private static void getProjects_returnsAllProjectsAsSummaries()
	{
		Mocks.ProjectSelector selector = new Mocks.ProjectSelector();

		selector.whenSelectAllActiveProjects().thenReturn( new List<sim_pm__Project__c>{ newProject('A'), newProject('B')} )
											  .thenAssertCalled(1);

		ProjectSelector.setImplementation(selector);

		List<KanbanController.ProjectSummary> result = KanbanController.getProjects();

		System.assertEquals(2, result.size(), 'Should return 2 project summaries');
		System.assertEquals('A', result[0].Name, 'Should return the first project');
		System.assertEquals('B', result[1].Name, 'Should return the second project');

		selector.assertCalls();
	}

	@isTest
	private static void getFeatures_returnsFeatures()
	{
		TestScenario data = TestScenario.createBasicScenario();

		List<KanbanController.FeatureSummary> result = KanbanController.getFeatures(data.Project.Id, data.Versions[0].Id);

		System.assertEquals(2, result.size(), 'Should return 2 feature summaries');
	}

	@isTest
	private static void updateTaskStatus_notFound_throwsException()
	{
		Mocks.ProjectSelector selector = new Mocks.ProjectSelector();

		selector.whenSelectTasksById().thenReturn( new List<Task__c>() )
									  .thenAssertCalled(1);

		ProjectSelector.setImplementation(selector);

		try
		{
			KanbanController.updateTaskStatus(null, 'In Progress');
			System.assert(false, 'Expected an exception but did not get one');
		}
		catch(Exception e)
		{
			System.assertEquals('Task not found', e.getMessage(), 'Unexpected exception thrown');
		}

		selector.assertCalls();
	}

	@isTest
	private static void updateTaskStatus_setsStatus()
	{
		TestScenario data = TestScenario.createBasicScenario();
		Id taskId = data.Tasks[0].Id;

		KanbanController.updateTaskStatus(taskId, 'Completed');

		Task__c task = [SELECT Id, Status__c FROM Task__c WHERE Id = :taskId];

		System.assertEquals('Completed', task.Status__c, 'Should update the task status');
	}

	@isTest
	private static void getTask_notFound_throwsException()
	{
		Mocks.ProjectSelector selector = new Mocks.ProjectSelector();

		selector.whenSelectTasksById().thenReturn( new List<Task__c>() )
									  .thenAssertCalled(1);

		ProjectSelector.setImplementation(selector);

		try
		{
			KanbanController.getTask(null);
			System.assert(false, 'Expected an exception but did not get one');
		}
		catch(Exception e)
		{
			System.assertEquals('Task not found', e.getMessage(), 'Unexpected exception thrown');
		}

		selector.assertCalls();
	}

	@isTest
	private static void getTask_returnsTask()
	{
		TestScenario data = TestScenario.createBasicScenario();
		Id taskId = data.Tasks[0].Id;

		KanbanController.TaskSummary task = KanbanController.getTask(taskId);

		System.assertEquals(taskId, task.Id, 'Should return the correct task');
	}

	@isTest
	private static void saveTask_updatesTask()
	{
		TestScenario data = TestScenario.createBasicScenario();
		String description = 'This is a new edited description';

		KanbanController.TaskSummary task = new KanbanController.TaskSummary(data.Tasks[0]);
		task.Description = description;

		KanbanController.saveTask(task);

		Task__c record = [SELECT Id, Description__c FROM Task__c WHERE Id = :task.Id];
		System.assertEquals(description, record.Description__c, 'Should have updated the record on the database');
	}

	@isTest
	private static void saveTask_insertsNewTask()
	{
		TestScenario data = TestScenario.createBasicScenario();

		KanbanController.TaskSummary task = new KanbanController.TaskSummary();
		task.Name = 'My new task';
		task.Feature = data.Features[0].Id;
		task.Description = 'This is a new task';
		task.Category = 'QA';

		KanbanController.saveTask(task);

		Task__c record = [SELECT Id, Description__c, Feature__c, Category__c FROM Task__c WHERE Name = 'My new task' LIMIT 1];
		System.assertEquals(data.Features[0].Id, record.Feature__c, 'Should have saved the feature');
		System.assertEquals('This is a new task', record.Description__c, 'Should have saved the description');
		System.assertEquals('QA', record.Category__c, 'Should have saved the category');
	}

	@isTest
	private static void getUsers_returnsUsers()
	{
		sim_pm__Project__c project = newProject('Test Project');
		insert project;

		Set<Id> userIds = new Set<Id>();
		userIds.add( TestHelper.createUser('Bob').Id );
		userIds.add( TestHelper.createUser('Fred').Id );
		userIds.add( TestHelper.createUser('Jack').Id );

		List<User> users = [SELECT Id, Name FROM User WHERE Id IN :userIds ORDER BY Name ASC];

		Mocks.ProjectSelector selector = new Mocks.ProjectSelector();
		selector.whenSelectProjectDevelopers()
			.thenReturn( users.clone() )
			.thenAssertCalled(1)
			.thenAssertCalledWith(project.Id);
		ProjectSelector.setImplementation(selector);

		List<KanbanController.UserSummary> summaries = KanbanController.getUsers(project.Id);

		selector.assertCalls();
		System.assertEquals(3, summaries.size(), 'Should return two summaries');
		System.assertEquals(users[0].Name, summaries[0].Name, 'Should return the first summary');
		System.assertEquals(users[1].Name, summaries[1].Name, 'Should return the second summary');
		System.assertEquals(users[2].Name, summaries[2].Name, 'Should return the third summary');
	}

	@isTest
	private static void startFeature_updatesFeatureStatus()
	{
		TestScenario scenario = TestScenario.createBasicScenario();
		Sim_pm__ProjectFeature__c feature = scenario.Features[2];

		System.assertEquals('Planning', feature.sim_pm__Status__c, 'SANITY -- Should be planning status');
		KanbanController.startFeature(feature.Id);

		feature = [SELECT sim_pm__Status__c FROM Sim_pm__ProjectFeature__c WHERE Id = :feature.Id LIMIT 1];
		System.assertEquals('In Progress', feature.sim_pm__Status__c, 'Should update the status');
	}

	@isTest
	private static void completeFeature_updatesFeatureStatus()
	{
		TestScenario scenario = TestScenario.createBasicScenario();
		Sim_pm__ProjectFeature__c feature = scenario.Features[0];

		System.assertEquals('In Progress', feature.sim_pm__Status__c, 'SANITY -- Should be in progress status');
		KanbanController.completeFeature(feature.Id);

		feature = [
			SELECT sim_pm__Status__c, sim_pm__Complete_Date__c
			FROM Sim_pm__ProjectFeature__c
			WHERE Id = :feature.Id
			LIMIT 1
		];

		System.assertEquals('Complete', feature.sim_pm__Status__c, 'Should update the status');
		System.assertEquals(Date.today(), feature.sim_pm__Complete_Date__c, 'Should set the complete date');
	}

	@isTest
	private static void projectSummary_copiesFieldsFromSObject()
	{
		TestScenario data = TestScenario.createBasicScenario();

		KanbanController.ProjectSummary summary = new KanbanController.ProjectSummary(data.Project);

		System.assertEquals(data.Project.Id, summary.Id, 'Should copy the Id field');
		System.assertEquals(data.Project.Name, summary.Name, 'Should copy the Name field');
		System.assertEquals(data.Project.sim_pm__Current_Version__c, summary.Version, 'Should copy the Name field');
	}

	@isTest
	private static void featureSummary_copiesFieldsFromSObject()
	{
		TestScenario data = TestScenario.createBasicScenario();
		sim_pm__ProjectFeature__c feature = data.Features[0];

		KanbanController.FeatureSummary summary = new KanbanController.FeatureSummary(feature);

		System.assertEquals(feature.Id, summary.Id, 'Should copy the Id field');
		System.assertEquals(feature.Name, summary.Name, 'Should copy the Name field');
		System.assertEquals(feature.sim_pm__Name__c, summary.Label, 'Should copy the Name__c field');
		System.assertEquals(feature.sim_pm__Status__c, summary.Status, 'Should copy the Status__c field');
		System.assertEquals(feature.sim_pm__Description__c, summary.Description, 'Should copy the Description__c field');
	}

	@isTest
	private static void taskSummary_copiesFieldsFromSObject()
	{
		TestScenario data = TestScenario.createBasicScenario();
		Task__c task = data.Tasks[0];

		KanbanController.TaskSummary summary = new KanbanController.TaskSummary(task);

		System.assertEquals(task.Id, summary.Id, 'Should copy the Id field');
		System.assertEquals(task.Name, summary.Name, 'Should copy the Name field');
		System.assertEquals(task.Category__c, summary.Category, 'Should copy the Category__c field');
		System.assertEquals(task.Description__c, summary.Description, 'Should copy the Description__c field');
		System.assertEquals(task.Estimate__c, summary.Estimate, 'Should copy the Estimate__c field');
		System.assertEquals(task.Feature__c, summary.Feature, 'Should copy the Feature__c field');
		System.assertEquals(task.Status__c, summary.Status, 'Should copy the Status__c field');
		System.assertEquals(task.TotalHours__c, summary.TotalHours, 'Should copy the TotalHours__c field');
	}

	@isTest
	private static void taskSummary_canCreateSObject()
	{
		KanbanController.TaskSummary summary = new KanbanController.TaskSummary();
		summary.Name = 'My Task';
		summary.Category = 'QA';
		summary.Description = 'This is my testing task';
		summary.Estimate = 6;
		summary.Status = 'In Progress';
		summary.TotalHours = 2;

		Task__c task = summary.toSObject();

		System.assertEquals(summary.Name, task.Name, 'Should copy the Name field');
		System.assertEquals(summary.Category, task.Category__c, 'Should copy the Category__c field');
		System.assertEquals(summary.Description, task.Description__c, 'Should copy the Description__c field');
		System.assertEquals(summary.Estimate, task.Estimate__c, 'Should copy the Estimate__c field');
		System.assertEquals(summary.Status, task.Status__c, 'Should copy the Status__c field');
		System.assertEquals(summary.TotalHours, task.TotalHours__c, 'Should copy the TotalHours__c field');
	}

	@isTest
	private static void userSummary_copiesFieldsFromSObject()
	{
		User bob = TestHelper.createUser('Bob');

		KanbanController.UserSummary summary = new KanbanController.UserSummary(bob);

		System.assertEquals(bob.Id, summary.Id, 'Should copy the Id field');
		System.assertEquals(bob.Name, summary.Name, 'Should copy the Name field');
	}

	private static sim_pm__Project__c newProject(String name)
	{
		return new sim_pm__Project__c(
			Name = name
		);
	}
}
