@isTest(SeeAllData=false)
public class Mocks
{
	public class ProjectSelector implements ProjectSelector.API
	{
		private sim_util.Mocks.Method mockSelectAllProjects = new sim_util.Mocks.Method('ProjectSelector', 'selectAllProjects');
		private sim_util.Mocks.Method mockSelectAllActivePrjects = new sim_util.Mocks.Method('ProjectSelector', 'selectAllActiveProjects');
		private sim_util.Mocks.Method mockSelectActiveFeaturesAndTasksByProject = new sim_util.Mocks.Method('ProjectSelector', 'selectActiveFeaturesAndTasksByProject');
		private sim_util.Mocks.Method mockSelectTasksById = new sim_util.Mocks.Method('ProjectSelector', 'selectTasksById');
		private sim_util.Mocks.Method mockSelectFeaturesById = new sim_util.Mocks.Method('ProjectSelector', 'selectFeaturesById');
		private sim_util.Mocks.Method mockSelectProjectDevelopers = new sim_util.Mocks.Method('ProjectSelector', 'selectProjectDevelopers');

		public void assertCalls()
		{
			mockSelectAllProjects.doAssertsNow();
			mockSelectAllActivePrjects.doAssertsNow();
			mockSelectActiveFeaturesAndTasksByProject.doAssertsNow();
			mockSelectTasksById.doAssertsNow();
			mockSelectFeaturesById.doAssertsNow();
			mockSelectProjectDevelopers.doAssertsNow();
		}

		public List<sim_pm__Project__c> selectAllProjects()
		{
			return (List<sim_pm__Project__c>)mockSelectAllProjects.call();
		}

		public sim_util.Mocks.Method whenSelectAllProjects()
		{
			return mockSelectAllProjects;
		}

		public List<sim_pm__Project__c> selectAllActiveProjects()
		{
			return (List<sim_pm__Project__c>)mockSelectAllActivePrjects.call();
		}

		public sim_util.Mocks.Method whenSelectAllActiveProjects()
		{
			return mockSelectAllActivePrjects;
		}

		public List<sim_pm__ProjectFeature__c> selectActiveFeaturesAndTasksByProject(Id projectId, Id version)
		{
			return (List<sim_pm__ProjectFeature__c>)mockSelectActiveFeaturesAndTasksByProject.call(new List<Object>{projectId, version});
		}

		public List<sim_pm__ProjectFeature__c> selectFeaturesById(Set<Id> ids)
		{
			return (List<sim_pm__ProjectFeature__c>)mockSelectFeaturesById.call(new List<Object>{ids});
		}

		public sim_util.Mocks.Method whenSelectFeatureById()
		{
			return mockSelectFeaturesById;
		}

		public sim_util.Mocks.Method whenSelectActiveFeaturesAndTasksByProject()
		{
			return mockSelectActiveFeaturesAndTasksByProject;
		}

		public List<Task__c> selectTasksById(Set<Id> ids)
		{
			return (List<Task__c>)mockSelectTasksById.call(new List<Object>{ids});
		}

		public sim_util.Mocks.Method whenSelectTasksById()
		{
			return mockSelectTasksById;
		}

		public List<User> selectProjectDevelopers(Id projectId)
		{
			return (List<User>)mockSelectProjectDevelopers.call(new List<Object>{projectId});
		}

		public sim_util.Mocks.Method whenSelectProjectDevelopers()
		{
			return mockSelectProjectDevelopers;
		}
	}
}
