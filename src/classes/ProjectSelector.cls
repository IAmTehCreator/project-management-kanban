public class ProjectSelector
{
	public static final String PROJECT_SOBJECT_NAME = 'sim_pm__Project__c';
	public static final String FEATURE_SOBJECT_NAME = 'sim_pm__ProjectFeature__c';
	public static final String TASK_SOBJECT_NAME = 'Task__c';

	private static final String PROJECT_ASSET_PURPOSE_DEVELOPER = 'Developer';

	public interface API
	{
		List<sim_pm__Project__c> selectAllProjects();
		List<sim_pm__Project__c> selectAllActiveProjects();

		List<sim_pm__ProjectFeature__c> selectFeaturesById(Set<Id> ids);
		List<sim_pm__ProjectFeature__c> selectActiveFeaturesAndTasksByProject(Id projectId, Id version);

		List<Task__c> selectTasksById(Set<Id> ids);

		List<User> selectProjectDevelopers(Id projectId);
	}

	private static ProjectSelector.API implementation;

	public static ProjectSelector.API newInstance()
	{
		return implementation == null ? new Selector() : implementation;
	}

	public static void setImplementation(ProjectSelector.API impl)
	{
		implementation = impl;
	}

	public class Selector extends SObjectSelector implements ProjectSelector.API
	{
		public List<sim_pm__Project__c> selectAllProjects()
		{
			return Database.query( buildQuery( getProjectFieldList(), PROJECT_SOBJECT_NAME ) );
		}

		public List<sim_pm__Project__c> selectAllActiveProjects()
		{
			List<String> activeStatuses = new List<String>{'Planning', 'In Progress'};

			return Database.query(
				buildQuery(
					getProjectFieldList(),
					PROJECT_SOBJECT_NAME,
					'sim_pm__Status__c IN :activeStatuses AND sim_pm__Current_Version__c != NULL'
				)
			);
		}

		public List<sim_pm__ProjectFeature__c> selectFeaturesById(Set<Id> ids)
		{
			if(ids == null || ids.isEmpty())
			{
				return new List<sim_pm__ProjectFeature__c>();
			}

			return Database.query( buildQuery( getFeatureFieldList(), FEATURE_SOBJECT_NAME, 'Id IN :ids') );
		}

		public List<sim_pm__ProjectFeature__c> selectActiveFeaturesAndTasksByProject(Id projectId, Id version)
		{
			if(projectId == null || version == null)
			{
				return new List<sim_pm__ProjectFeature__c>();
			}

			List<String> activeStatuses = new List<String>{'Planning', 'In Progress'};

			return [
				SELECT Id, Name, sim_pm__Name__c, sim_pm__Complete_Date__c, sim_pm__Description__c,
					sim_pm__Estimated_Dev_Hours__c, sim_pm__Project__c, sim_pm__Release__c,
					sim_pm__Remaining_Dev_Hours__c, sim_pm__Start_Date__c, sim_pm__Status__c,
					sim_pm__Total_Dev_Hours__c, sim_pm__Assigned_User__c,
					(
						SELECT Id, Name, Description__c, Assigned__c, Assigned__r.Name, Estimate__c, Feature__c,
							Status__c, TotalHours__c, Category__c
						FROM Tasks__r
					)
				FROM sim_pm__ProjectFeature__c
				WHERE sim_pm__Project__c = :projectId
					AND sim_pm__Status__c IN :activeStatuses
					AND sim_pm__Release__c = :version
				ORDER BY Name ASC
			];
		}

		public List<Task__c> selectTasksById(Set<Id> ids)
		{
			if ( ids == null || ids.isEmpty() )
			{
				return new List<Task__c>();
			}

			return Database.query( buildQuery( getTaskFieldList(), TASK_SOBJECT_NAME, 'Id IN :ids' ) );
		}

		public List<User> selectProjectDevelopers(Id projectId)
		{
			if(projectId == null)
			{
				return new List<User>();
			}

			return [
				SELECT Id, Name
				FROM User
				WHERE Id IN (
					SELECT sim_pm__Person__c
					FROM sim_pm__ProjectAsset__c
					WHERE sim_pm__Project__c = :projectId
						AND sim_pm__Purpose__c = :PROJECT_ASSET_PURPOSE_DEVELOPER
						AND sim_pm__Assigned__c = TRUE
				)
				ORDER BY Name ASC
			];
		}

		private List<String> getProjectFieldList()
        {
        	List<String> fields = new List<String>();
            fields.add('Name');
            fields.add('sim_pm__Budget__c');
            fields.add('sim_pm__Complete_Date__c');
            fields.add('sim_pm__Completed_Features__c');
            fields.add('sim_pm__Contract__c');
            fields.add('sim_pm__Current_Bugs__c');
            fields.add('sim_pm__Current_Version__c');
            fields.add('sim_pm__Customer__c');
            fields.add('sim_pm__Description__c');
            fields.add('sim_pm__Features__c');
            fields.add('sim_pm__Manager_User__c');
            fields.add('sim_pm__Manager_Contact__c');
            fields.add('sim_pm__Platform__c');
            fields.add('sim_pm__Product__c');
            fields.add('sim_pm__Remaining_Bug_Hours__c');
            fields.add('sim_pm__Remaining_Feature_Hours__c');
            fields.add('sim_pm__Repository__c');
            fields.add('sim_pm__Source_Control__c');
            fields.add('sim_pm__Started_Date__c');
            fields.add('sim_pm__Status__c');
            fields.add('sim_pm__Total_Asset_Value__c');
            fields.add('sim_pm__Total_Staff__c');
            fields.add('sim_pm__Type__c');
            fields.add('sim_pm__Website__c');
            return fields;
        }

		private List<String> getFeatureFieldList()
		{
			return new List<String>{
				'Id',
				'Name',
				'sim_pm__Name__c',
				'sim_pm__Complete_Date__c',
				'sim_pm__Description__c',
				'sim_pm__Estimated_Dev_Hours__c',
				'sim_pm__Project__c',
				'sim_pm__Release__c',
				'sim_pm__Remaining_Dev_Hours__c',
				'sim_pm__Start_Date__c',
				'sim_pm__Status__c',
				'sim_pm__Total_Dev_Hours__c',
				'sim_pm__Assigned_User__c'
			};
		}

		private List<String> getTaskFieldList()
		{
			List<String> fields = new List<String>();
			fields.add('Id');
			fields.add('Name');
			fields.add('Assigned__c');
			fields.add('Assigned__r.Name');
			fields.add('Category__c');
			fields.add('Description__c');
			fields.add('Estimate__c');
			fields.add('Feature__c');
			fields.add('Status__c');
			fields.add('TotalHours__c');
			return fields;
		}
	}
}
