@isTest(SeeAllData=false)
private class SObjectSelectorTest
{
    private static TestMethod void buildQueryReturnsQueryString()
    {
        TestSelector selector = new TestSelector();
        String query = selector.doBuildQuery(new List<String>{'Id', 'Name', 'Other__c'}, 'Test__c');

        System.assertEquals('SELECT Id, Name, Other__c FROM Test__c', query, 'Should have generated the correct query string');
    }

    private static TestMethod void buildQueryWithWhereClauseReturnsQueryString()
    {
        TestSelector selector = new TestSelector();
        String query = selector.doBuildQuery(new List<String>{'Id', 'Name', 'Other__c'}, 'Test__c', 'Name = :Test');

        System.assertEquals('SELECT Id, Name, Other__c FROM Test__c WHERE Name = :Test', query, 'Should have generated the correct query string');
    }

    private static TestMethod void getFieldStringReturnsFieldsAsString()
    {
        TestSelector selector = new TestSelector();
        String fields = selector.doGetFieldString(new List<String>{'Id', 'Name', 'Custom__c'});

        System.assertEquals('Id, Name, Custom__c', fields, 'Should return a string containing comma separated field names');
    }

    private static TestMethod void queryBuilderBuildsSimpleQuery()
    {
        SObjectSelector.QueryBuilder query = new SObjectSelector.QueryBuilder();

        query.selectSObject('Test__c')
             .withFields(new List<String>{'Id', 'Name', 'Custom__c'});

        System.assertEquals('SELECT Id, Name, Custom__c FROM Test__c', query.getQuery(), 'Should build correct query string');
    }

    private static TestMethod void queryBuilderBuildsWhereClause()
    {
        SObjectSelector.QueryBuilder query = new SObjectSelector.QueryBuilder();

        query.selectSObject('Test__c')
             .withFields(new List<String>{'Id', 'Name', 'Custom__c'})
             .whereField('Name')
             .isEqualTo('Test');

        System.assertEquals('SELECT Id, Name, Custom__c FROM Test__c WHERE Name = \'Test\'', query.getQuery(), 'Should build correct query string');
    }

    private static TestMethod void queryBuilderBuildsWhereClauseWithList()
    {
        SObjectSelector.QueryBuilder query = new SObjectSelector.QueryBuilder();

        query.selectSObject('Test__c')
             .withFields(new List<String>{'Id', 'Name', 'Custom__c'})
             .whereField('Custom__c')
             .inList(new List<Object>());

        System.assertEquals('SELECT Id, Name, Custom__c FROM Test__c WHERE Custom__c IN :QueryFilterList', query.getQuery(), 'Should build correct query string');
    }

    private static TestMethod void queryBuilderBuildsWhereClauseWithAndOperator()
    {
        SObjectSelector.QueryBuilder query = new SObjectSelector.QueryBuilder();

        query.selectSObject('Test__c')
             .withFields(new List<String>{'Id', 'Name', 'Custom__c'})
             .whereField('Custom__c')
             .inList(new List<Object>())
             .andField('Other__c')
             .isEqualTo('Test');

        System.assertEquals('SELECT Id, Name, Custom__c FROM Test__c WHERE Custom__c IN :QueryFilterList AND Other__c = \'Test\'', query.getQuery(), 'Should build correct query string');
    }

    private static TestMethod void queryBuilderBuildsWhereClauseWithOrOperator()
    {
        SObjectSelector.QueryBuilder query = new SObjectSelector.QueryBuilder();

        query.selectSObject('Test__c')
             .withFields(new List<String>{'Id', 'Name', 'Custom__c'})
             .whereField('Custom__c')
             .inList(new List<Object>())
             .orField('Other__c')
             .isEqualTo('Test');

        System.assertEquals('SELECT Id, Name, Custom__c FROM Test__c WHERE Custom__c IN :QueryFilterList OR Other__c = \'Test\'', query.getQuery(), 'Should build correct query string');
    }

    private class TestSelector extends SObjectSelector
    {
        public String doBuildQuery(List<String> fields, String sObjName)
        {
            return buildQuery(fields, sObjName);
        }

        public String doBuildQuery(List<String> fieldList, String sObjName, String whereClause)
        {
            return buildQuery(fieldList, sObjName, whereClause);
        }

        public String doGetFieldString(List<String> fields)
        {
            return getFieldString(fields);
        }
    }

}
