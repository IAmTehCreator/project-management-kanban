@isTest(SeeAllData=false)
public class TaskTriggerHandlerTest
{
	@isTest
	public static void recordDiff_returnsOldAndNewRecords()
	{
		Task__c oldRecord = new Task__c();
		Task__c newRecord = new Task__c();

		TaskTriggerHandler.RecordDiff diff =  new TaskTriggerHandler.RecordDiff(oldRecord, newRecord);

		System.assertEquals(oldRecord, diff.getOld(), 'Should return the old record');
		System.assertEquals(newRecord, diff.getNew(), 'Should return the new record');
	}

	@isTest
	public static void recordDiff_getTotalTimeDelta_shouldReturnDifferenceInTotalTime()
	{
		Task__c oldRecord = new Task__c();
		Task__c newRecord = new Task__c();

		oldRecord.TotalHours__c = 2;
		newRecord.TotalHours__c = 5;

		TaskTriggerHandler.RecordDiff diff =  new TaskTriggerHandler.RecordDiff(oldRecord, newRecord);

		System.assertEquals(3, diff.getTotalTimeDelta(), 'Should return the difference in time');
	}

	@isTest
	public static void updateRemainingFeatureTime_shouldUpdateFeature()
	{
		TestScenario data = TestScenario.createBasicScenario();
		Task__c oldTask = data.Tasks[0];
		Task__c newTask = data.Tasks[0].clone(true, true, true, true);

		oldTask.TotalHours__c = 1;
		newTask.TotalHours__c = 3;

		TaskTriggerHandler handler = new TaskTriggerHandler(new List<Task__c>{oldTask}, new List<Task__c>{newTask});
		handler.updateRemainingFeatureTime();

		sim_pm__ProjectFeature__c feature = [
			SELECT sim_pm__Total_Dev_Hours__c, sim_pm__Remaining_Dev_Hours__c
			FROM sim_pm__ProjectFeature__c
			WHERE Id = :data.Features[0].Id
			LIMIT 1
		];

		System.assertEquals(2, feature.sim_pm__Total_Dev_Hours__c, 'Should increment the total development hours by the difference');
		System.assertEquals(0, feature.sim_pm__Remaining_Dev_Hours__c, 'Should decrement the remaining dev hours and cap at 0');
	}
}
