public with sharing class KanbanController
{
	private static final String FEATURE_STATUS_PLANNING = 'Planning';
	private static final String FEATURE_STATUS_IN_PROGRESS = 'In Progress';
	private static final String FEATURE_STATUS_COMPLETE = 'Complete';

	@RemoteAction
	public static List<ProjectSummary> getProjects()
	{
		return wrapProjects( ProjectSelector.newInstance().selectAllActiveProjects() );
	}

	@RemoteAction
	public static List<FeatureSummary> getFeatures(Id projectId, Id versionId)
	{
		return wrapFeatures( ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(projectId, versionId) );
	}

	@RemoteAction
	public static void updateTaskStatus(Id taskId, String status)
	{
		Task__c task = selectTask(taskId);
		task.Status__c = status;

		update task;
	}

	@RemoteAction
	public static TaskSummary getTask(Id taskId)
	{
		return new TaskSummary( selectTask(taskId) );
	}

	@RemoteAction
	public static void saveTask(TaskSummary task)
	{
		upsert task.toSObject();
	}

	@RemoteAction
	public static List<UserSummary> getUsers(Id projectId)
	{
		return wrapUsers( ProjectSelector.newInstance().selectProjectDevelopers(projectId) );
	}

	@RemoteAction
	public static void startFeature(Id featureId)
	{
		Sim_pm__ProjectFeature__c feature = ProjectSelector.newInstance().selectFeaturesById(new Set<Id>{featureId})[0];
		feature.sim_pm__Status__c = FEATURE_STATUS_IN_PROGRESS;

		update feature;
	}

	@RemoteAction
	public static void completeFeature(Id featureId)
	{
		Sim_pm__ProjectFeature__c feature = ProjectSelector.newInstance().selectFeaturesById(new Set<Id>{featureId})[0];
		feature.sim_pm__Status__c = FEATURE_STATUS_COMPLETE;
		feature.sim_pm__Complete_Date__c = Date.today();

		update feature;
	}

	private static Task__c selectTask(Id taskId)
	{
		List<Task__c> tasks = ProjectSelector.newInstance().selectTasksById( new Set<Id>{taskId} );

		if(tasks.size() < 1)
		{
			throw new KanbanException(Label.TaskNotFoundExceptionText);
		}

		return tasks.get(0);
	}

	private static List<ProjectSummary> wrapProjects(List<sim_pm__Project__c> projects)
	{
		List<ProjectSummary> summaries = new List<ProjectSummary>();
		for(sim_pm__Project__c project : projects)
		{
			summaries.add( new ProjectSummary(project) );
		}

		return summaries;
	}

	private static List<FeatureSummary> wrapFeatures(List<sim_pm__ProjectFeature__c> features)
	{
		List<FeatureSummary> summaries = new List<FeatureSummary>();
		for(sim_pm__ProjectFeature__c feature : features)
		{
			summaries.add( new FeatureSummary(feature) );
		}

		return summaries;
	}

	private static List<UserSummary> wrapUsers(List<User> users)
	{
		List<UserSummary> summaries = new List<UserSummary>();
		for(User u : users)
		{
			summaries.add( new UserSummary(u) );
		}

		return summaries;
	}

	public class ProjectSummary
	{
		public Id Id {get; set;}
		public String Name {get; set;}
		public Id Version {get; set;}

		public ProjectSummary(sim_pm__Project__c project)
		{
			this.Id = project.Id;
			this.Name = project.Name;
			this.Version = project.sim_pm__Current_Version__c;
		}
	}

	public class FeatureSummary
	{
		public Id Id {get; set;}
		public String Name {get; set;}
		public String Label {get; set;}
		public String Status {get; set;}
		public String Description {get; set;}

		public List<TaskSummary> Tasks {get; set;}

		public FeatureSummary(sim_pm__ProjectFeature__c feature)
		{
			this.Id = feature.Id;
			this.Name = feature.Name;
			this.Label = feature.sim_pm__Name__c;
			this.Status = feature.sim_pm__Status__c;
			this.Description = feature.sim_pm__Description__c;

			this.Tasks = new List<TaskSummary>();

			for ( Task__c task : feature.Tasks__r)
			{
				this.Tasks.add( new TaskSummary(task) );
			}
		}
	}

	public class TaskSummary
	{
		public Id Id {get; set;}
		public String Name {get; set;}
		public String Category {get; set;}
		public String Description {get; set;}
		public Id Assigned {get; set;}
		public String AssignedName {get; set;}
		public Decimal Estimate {get; set;}
		public String Feature {get; set;}
		public String Status {get; set;}
		public Decimal TotalHours {get; set;}

		public TaskSummary() {}

		public TaskSummary(Task__c task)
		{
			this.Id = task.Id;
			this.Name = task.Name;
			this.Category = task.Category__c;
			this.Description = task.Description__c;
			this.Assigned = task.Assigned__c;
			this.AssignedName = task.Assigned__r.Name;
			this.Estimate = task.Estimate__c;
			this.Feature = task.Feature__c;
			this.Status = task.Status__c;
			this.TotalHours = task.TotalHours__c;
		}

		public Task__c toSObject()
		{
			return new Task__c(
				Id = this.Id,
				Name = this.Name,
				Category__c = this.Category,
				Description__c = this.Description,
				Assigned__c = this.Assigned,
				Estimate__c = this.Estimate,
				Status__c = this.Status,
				TotalHours__c = this.TotalHours,
				Feature__c = this.Feature
			);
		}
	}

	public class UserSummary
	{
		public Id Id {get; set;}
		public String Name {get; set;}

		public UserSummary() {}

		public UserSummary(User sObj)
		{
			this.Id = sObj.Id;
			this.Name = sObj.Name;
		}
	}
}
