@isTest(SeeAllData=false)
public class TestHelper
{
	private static final String PROFILE_STANDARD_USER = 'Standard User';
	private static Id standardUserProfile;

	public static User createUser(String name)
	{
		User projectUser = new User(
			Email = name + '.test@example.com',
			Alias = name.left(8),
			IsActive = true,
			FirstName = name,
			LastName = 'User',
			Username = name + '.kanban.test@example.com',
			CommunityNickname = name,
			TimeZoneSidKey = 'GMT',
			LocaleSidKey = 'en_GB',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			ProfileId = getStandardUserProfile()
		);

		insert projectUser;

		return projectUser;
	}

	public static Id getStandardUserProfile()
	{
		if(standardUserProfile == null)
		{
			standardUserProfile = [SELECT Id FROM Profile WHERE Name = :PROFILE_STANDARD_USER LIMIT 1].Id;
		}

		return standardUserProfile;
	}
}
