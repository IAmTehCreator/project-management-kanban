public abstract class SObjectSelector
{

    protected String buildQuery(List<String> fieldList, String sObjName)
    {
        return buildQuery(fieldList, sObjName, null);
    }

	protected String buildQuery(List<String> fieldList, String sObjName, String whereClause)
    {
        String query = 'SELECT ';
        query += getFieldString(fieldList);
        query += ' FROM ';
        query += sObjName;

        if(whereClause != null && whereClause != '')
        {
            query += ' WHERE ' + whereClause;
        }

        return query;
    }

    protected String getFieldString(List<String> fields)
    {
        String fieldString = '';
        for(String field : fields)
        {
            if(fieldString == '')
            {
                fieldString = field;
            }
            else
            {
                fieldString = fieldString + ', ' + field;
            }
        }
        return fieldString;
    }

    /**
    private void queryBuilderExample()
    {
        QueryBuilder query = new QueryBuilder();
        List<Project__c> projs = (List<Project__c>)query.selectSObject('Project__c')
             											.withFields(new List<String>{'Id', 'Name'})
             											.whereField('Name')
             											.isEqualTo('Project Management Package')
             											.andField('Status__c')
             											.inList(new List<String>{'Planning', 'In Progress'})
             											.queryNow();
        //SELECT Id, Name FROM Project__c WHERE Name = 'Project Management Package' AND Status__c IN :QueryFilterList
    }
	*/

    public class QueryBuilder
    {
        public String QueryObject {get; set;}
        public List<String> QueryFields {get; set;}
        public String QueryFilter {get; set;}
        public List<Object> QueryFilterList {get; set;}

        public String QueryString {get; set;}

        public QueryBuilder()
        {
			QueryFields = new List<String>();
            QueryFilterList = new List<Object>();
        }

        public QueryBuilder selectSObject(String sObjName)
        {
            QueryObject = sObjName;
            return this;
        }

        public QueryBuilder withFields(List<String> fields)
        {
            QueryFields = fields;
            return this;
        }

        public QueryBuilder whereField(String fieldName)
        {
            if(QueryFilter == null || QueryFilter == '')
                QueryFilter = ' WHERE';

            QueryFilter += ' ' + fieldName;
            return this;
        }

        public QueryBuilder isEqualTo(String value)
        {
            QueryFilter += ' = \'' + value + '\'';
            return this;
        }

        public QueryBuilder inList(List<Object> filterList)
        {
            QueryFilterList = filterList;
            QueryFilter += ' IN :QueryFilterList';
            return this;
        }

        public QueryBuilder andField(String fieldName)
        {
            QueryFilter += ' AND ' + fieldName;
            return this;
        }

        public QueryBuilder orField(String fieldName)
        {
            QueryFilter += ' OR ' + fieldName;
            return this;
        }

        public List<SObject> queryNow()
        {
            return Database.query( getQuery() );
        }

        public String getQuery()
        {
            QueryString = 'SELECT ' + getFieldString(QueryFields) + ' FROM ' + QueryObject;
            if(QueryFilter != null && QueryFilter != '')
                QueryString += QueryFilter;

            return QueryString;
        }

        private String getFieldString(List<String> fields)
        {
            String fieldString = '';
            for(String field : fields)
            {
                if(fieldString == '')
                {
                    fieldString = field;
                }
                else
                {
                    fieldString = fieldString + ', ' + field;
                }
            }
            return fieldString;
        }
    }
}
