@isTest(SeeAllData=false)
public class TestScenario
{
	public sim_pm__Project__c Project {get; set;}
	public List<sim_pm__ProjectVersion__c> Versions {get; set;}
	public List<sim_pm__ProjectFeature__c> Features {get; set;}
	public List<Task__c> Tasks {get; set;}

	public TestScenario()
	{
		Features = new List<sim_pm__ProjectFeature__c>();
		Versions = new List<sim_pm__ProjectVersion__c>();
		Tasks = new List<Task__c>();
	}

	public static TestScenario createBasicScenario()
	{
		TestScenario data = new TestScenario();

		data.Project = new sim_pm__Project__c(
			Name = 'Test project'
		);

		insert data.Project;

		data.Versions.add( new sim_pm__ProjectVersion__c(
			Name = '1.0',
			sim_pm__Current__c = true,
			sim_pm__Project__c = data.Project.Id
		));

		data.Versions.add( new sim_pm__ProjectVersion__c(
			Name = '1.1',
			sim_pm__Project__c = data.Project.Id
		));

		insert data.Versions;

		data.Features.add( new sim_pm__ProjectFeature__c(
			sim_pm__Project__c = data.Project.Id,
			sim_pm__Name__c = 'My Feature',
			sim_pm__Release__c = data.Versions[0].Id,
			sim_pm__Description__c = 'This is my new feature',
			sim_pm__Status__c = 'In Progress',
			sim_pm__Remaining_Dev_Hours__c = 1
		));

		data.Features.add( new sim_pm__ProjectFeature__c(
			sim_pm__Project__c = data.Project.Id,
			sim_pm__Name__c = 'Another Feature',
			sim_pm__Release__c = data.Versions[1].Id,
			sim_pm__Description__c = 'Ignore this feature, it is not going anywhere',
			sim_pm__Status__c = 'Pending'
		));

		data.Features.add( new sim_pm__ProjectFeature__c(
			sim_pm__Project__c = data.Project.Id,
			sim_pm__Name__c = 'Implement a thing',
			sim_pm__Release__c = data.Versions[0].Id,
			sim_pm__Description__c = 'Implement something',
			sim_pm__Status__c = 'Planning'
		));

		insert data.Features;

		data.Tasks.add( new Task__c(
			Name = 'Write unit tests',
			Category__c = 'Development',
			Description__c = 'Write unit tests to improve confidence in code quality',
			Estimate__c = 3,
			Status__c = 'In Progress',
			Feature__c = data.Features[0].Id
		));

		data.Tasks.add( new Task__c(
			Name = 'Null pointer exception',
			Category__c = 'Bug',
			Description__c = 'A null pointer exception is thrown when running the process',
			Estimate__c = 6,
			Status__c = 'Not Started',
			Feature__c = data.Features[0].Id
		));

		data.Tasks.add( new Task__c(
			Name = 'Manual Test',
			Category__c = 'QA',
			Description__c = 'Run a manual test to ensure feature works',
			Estimate__c = 12,
			Status__c = 'Not Started',
			Feature__c = data.Features[1].Id
		));

		insert data.Tasks;

		return data;
	}
}
