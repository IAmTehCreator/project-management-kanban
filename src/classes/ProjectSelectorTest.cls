@isTest(SeeAllData=false)
public class ProjectSelectorTest
{

	@isTest
	private static void newInstance_notMocked_shouldReturnNewInstance()
	{
		System.assert(ProjectSelector.newInstance() instanceof ProjectSelector.Selector, 'Should return a new selector implementation');
	}

	@isTest
	private static void newInstance_mocked_shouldReturnMock()
	{
		Mocks.ProjectSelector mock = new Mocks.ProjectSelector();
		ProjectSelector.setImplementation(mock);

		System.assert(ProjectSelector.newInstance() === mock, 'Should return the mock implementation');
	}

	@isTest
	private static void selectAllProjects_shouldReturnAllProjects()
	{
		insertProjectsWithVersion('1.0.0', new List<sim_pm__Project__c>{
			newProject('A'),
			newProject('B'),
			newProject('C')
		});

		List<sim_pm__Project__c> projects = ProjectSelector.newInstance().selectAllProjects();

		System.assertEquals(3, projects.size(), 'Should return all three projects');
	}

	@isTest
	private static void selectAllActiveProjects_shouldReturnAllActiveProjects()
	{
		Set<String> activeStatuses = new Set<String>{'Planning', 'In Progress'};

		insertProjectsWithVersion('1.0.0', new List<sim_pm__Project__c>{
			newProject('A', 'Pending'),
			newProject('B', 'Planning'),
			newProject('C', 'In Progress'),
			newProject('D', 'Abandoned'),
			newProject('E', 'Complete'),
			newProject('F', 'Deprecated')
		});

		List<sim_pm__Project__c> projects = ProjectSelector.newInstance().selectAllActiveProjects();

		System.assertEquals(2, projects.size(), 'Should return two projects');
		System.assert( activeStatuses.contains(projects[0].sim_pm__Status__c), 'Should return an active project' );
		System.assert( activeStatuses.contains(projects[1].sim_pm__Status__c), 'Should return an active project' );
	}

	@isTest
	private static void selectAllActiveProjects_shouldNotReturnProjectsWithoutCurrentVersion()
	{
		insertProjectsWithVersion('1.0.0', new List<sim_pm__Project__c>{
			newProject('A', 'Planning'),
			newProject('B', 'In Progress')
		});

		insert new List<sim_pm__Project__c>{
			newProject('C', 'Planning'),
			newProject('D', 'In Progress')
		};

		List<sim_pm__Project__c> projects = ProjectSelector.newInstance().selectAllActiveProjects();

		System.assertEquals(2, projects.size(), 'Should only return two projects');
		System.assert(projects[0].sim_pm__Current_Version__c != null, 'First project should have a current version');
		System.assert(projects[1].sim_pm__Current_Version__c != null, 'Second project should have a current version');
	}

	@isTest
	private static void selectFeaturesById_passedNull_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectFeaturesById(null);

		System.assertEquals(new List<sim_pm__ProjectFeature__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectFeaturesById_passedEmptySet_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectFeaturesById(new Set<Id>());

		System.assertEquals(new List<sim_pm__ProjectFeature__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectFeaturesById_returnsFeatures()
	{
		TestScenario data = TestScenario.createBasicScenario();
		Set<Id> ids = new Set<Id>{ data.Features[0].Id, data.Features[1].Id };

		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectFeaturesById(ids);

		System.assertEquals(2, result.size(), 'Should query two records');
		System.assert(ids.contains(result[0].Id), 'Should return record with desired ID');
		System.assert(ids.contains(result[1].Id), 'Should return record with desired ID');
	}

	@isTest
	private static void selectActiveFeaturesAndTasksByProject_passedNullProject_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		// TODO - Get a version ID
		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(null, null);

		System.assertEquals(new List<sim_pm__ProjectFeature__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectActiveFeaturesAndTasksByProject_passedNullVersion_doesNotQuery()
	{
		sim_pm__Project__c project = new sim_pm__Project__c( Name = 'Test Project' );
		insert project;

		Integer queries = Limits.getQueries();
		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(project.Id, null);

		System.assertEquals(new List<sim_pm__ProjectFeature__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectActiveFeaturesAndTasksByProject_passedNulls_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(null, null);

		System.assertEquals(new List<sim_pm__ProjectFeature__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectActiveFeaturesAndTasksByProject_returnsOnlyActiveFeatures()
	{
		TestScenario data = TestScenario.createBasicScenario();

		Id projectId = data.Project.Id;
		Id versionId = data.Versions[0].Id;
		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(projectId, versionId);

		System.assertEquals(2, result.size(), 'Should return one feature');
		System.assertEquals('In Progress', result[0].sim_pm__Status__c, 'Features with "In Progress" status should be returned');
		System.assertEquals('Planning', result[1].sim_pm__Status__c, 'Features with "Planning" status should be returned');
	}

	@isTest
	private static void selectActiveFeaturesAndTasksByProject_returnsOnlyFeaturesFromCurrentVersion()
	{
		TestScenario data = TestScenario.createBasicScenario();

		Id projectId = data.Project.Id;
		Id versionId = data.Versions[0].Id;
		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(projectId, versionId);

		System.assertEquals(2, result.size(), 'Should return one feature');
		System.assertEquals(versionId, result[0].sim_pm__Release__c, 'Returned feature should belong to the current version');
		System.assertEquals(versionId, result[1].sim_pm__Release__c, 'Returned feature should belong to the current version');
	}

	@isTest
	private static void selectActiveFeaturesAndTasksByProject_returnsAllTasks()
	{
		TestScenario data = TestScenario.createBasicScenario();

		Id projectId = data.Project.Id;
		Id versionId = data.Versions[0].Id;
		List<sim_pm__ProjectFeature__c> result = ProjectSelector.newInstance().selectActiveFeaturesAndTasksByProject(projectId, versionId);

		System.assertEquals(2, result.size(), 'Should return one feature');
		System.assertEquals(2, result[0].Tasks__r.size(), 'Should return two feature tasks');
	}

	@isTest
	private static void selectTasksById_passedNull_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		List<Task__c> result = ProjectSelector.newInstance().selectTasksById(null);

		System.assertEquals(new List<Task__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectTasksById_passedEmptySet_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		List<Task__c> result = ProjectSelector.newInstance().selectTasksById(new Set<Id>());

		System.assertEquals(new List<Task__c>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectTasksById_returnsTasks()
	{
		TestScenario data = TestScenario.createBasicScenario();

		Set<Id> ids = new Set<Id>{ data.Tasks[0].Id, data.Tasks[2].Id };

		List<Task__c> result = ProjectSelector.newInstance().selectTasksById(ids);

		System.assertEquals(2, result.size(), 'Should return two tasks');
	}

	@isTest
	private static void selectProjectDevelopers_passedNullId_doesNotQuery()
	{
		Integer queries = Limits.getQueries();

		List<User> result = ProjectSelector.newInstance().selectProjectDevelopers(null);

		System.assertEquals(new List<User>(), result, 'Should return an empty list');
		System.assertEquals(queries, Limits.getQueries(), 'Should not query the database');
	}

	@isTest
	private static void selectProjectDevelopers_returnsUsers()
	{
		sim_pm__Project__c project = newProject('Test Project');
		sim_pm__Project__c otherProject = newProject('Other Project');
		insert new List<sim_pm__Project__c>{project, otherProject};

		insert new List<sim_pm__ProjectAsset__c>{
			createAsset(project.Id, 'Bob', 'Developer', true),			// Included
			createAsset(project.Id, 'Joe', 'Support', true),			// Excluded
			createAsset(project.Id, 'Ziggy', 'Developer', true),		// Included
			createAsset(otherProject.Id, 'Dave', 'Developer', true),	// Excluded
			createAsset(project.Id, 'Jack', 'Developer', false)			// Excluded
		};

		List<User> users = ProjectSelector.newInstance().selectProjectDevelopers(project.Id);

		System.assertEquals(2, users.size(), 'Should only return two users');
		System.assertEquals('Bob User', users[0].Name, 'Should only return active developers');
		System.assertEquals('Ziggy User', users[1].Name, 'Should be sorted by name');
	}

	private static sim_pm__ProjectAsset__c createAsset(Id project, String name, String purpose, Boolean assigned)
	{
		return new sim_pm__ProjectAsset__c(
			sim_pm__Person__c = TestHelper.createUser(name).Id,
			sim_pm__Assigned__c = assigned,
			sim_pm__Purpose__c = purpose,
			sim_pm__Project__c = project
		);
	}

	private static sim_pm__Project__c newProject(String name)
	{
		return newProject(name, 'In Progress');
	}

	private static sim_pm__Project__c newProject(String name, String status)
	{
		return new sim_pm__Project__c(
			Name = name,
			sim_pm__Status__c = status
		);
	}

	private static void insertProjectsWithVersion(String versionName, List<sim_pm__Project__c> projects)
	{
		insert projects;

		List<sim_pm__ProjectVersion__c> versions = new List<sim_pm__ProjectVersion__c>();
		for(sim_pm__Project__c project : projects)
		{
			versions.add(new sim_pm__ProjectVersion__c(
				sim_pm__Project__c = project.Id,
				sim_pm__Current__c = true,
				Name = versionName
			));
		}

		insert versions;

		for(Integer i = 0; i < projects.size(); i++)
		{
			// Remove deprecated projects from the list as they can no longer be
			// updated.
			if(projects[i].sim_pm__Status__c == 'Deprecated')
			{
				projects.remove(i);
				continue;
			}

			projects[i].sim_pm__Current_Version__c = versions[i].Id;
		}

		update projects;
	}
}
