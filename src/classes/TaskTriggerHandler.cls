public class TaskTriggerHandler
{
	private Map<Id, RecordDiff> records;
	private Set<Id> featureIds;

	public TaskTriggerHandler(List<Task__c> oldRecords, List<Task__c> newRecords)
	{
		Map<Id, Task__c> oldMap = new Map<Id, Task__c>(oldRecords);
		Map<Id, Task__c> newMap = new Map<Id, Task__c>(newRecords);

		records = new Map<Id, RecordDiff>();
		featureIds = new Set<Id>();
		for(Id recordId : oldMap.keySet())
		{
			Task__c oldRecord = oldMap.get(recordId);
			Task__c newRecord = newMap.get(recordId);

			records.put(recordId, new RecordDiff(oldRecord, newRecord));
			featureIds.add(newRecord.Feature__c);
		}
	}

	public TaskTriggerHandler updateRemainingFeatureTime()
	{
		Map<Id, sim_pm__ProjectFeature__c> features = getAllParentFeatures();

		for(Id recordId : records.keySet())
		{
			RecordDiff record = records.get(recordId);
			Decimal timeDiff = record.getTotalTimeDelta();

			sim_pm__ProjectFeature__c feature = features.get(record.getNew().Feature__c);
			feature.sim_pm__Total_Dev_Hours__c += timeDiff;
			feature.sim_pm__Remaining_Dev_Hours__c -= timeDiff;

			if(feature.sim_pm__Remaining_Dev_Hours__c <= 0)
			{
				feature.sim_pm__Remaining_Dev_Hours__c = 0;
			}
		}

		update features.values();

		return this;
	}

	private Map<Id, sim_pm__ProjectFeature__c> getAllParentFeatures()
	{
		return new Map<Id, sim_pm__ProjectFeature__c>(
			ProjectSelector.newInstance().selectFeaturesById(featureIds)
		);
	}

	public class RecordDiff
	{
		private Task__c oldRecord;
		private Task__c newRecord;

		public RecordDiff(Task__c oldRecord, Task__c newRecord)
		{
			this.oldRecord = oldRecord;
			this.newRecord = newRecord;
		}

		public Task__c getOld()
		{
			return oldRecord;
		}

		public Task__c getNew()
		{
			return newRecord;
		}

		public Decimal getTotalTimeDelta()
		{
			Decimal oldHours = getOld().TotalHours__c;
			Decimal newHours = getNew().TotalHours__c;

			oldHours = oldHours == null ? 0 : oldHours;
			newHours = newHours == null ? 0 : newHours;

			return newHours - oldHours;
		}
	}
}
