trigger TaskTrigger on Task__c (before update)
{
	TaskTriggerHandler handler = new TaskTriggerHandler(Trigger.old, Trigger.new);
	handler.updateRemainingFeatureTime();
}
